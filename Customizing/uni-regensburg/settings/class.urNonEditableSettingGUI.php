<?php

class urNonEditableSettingGUI {

	private $id;
	private $title;
	private $value;

	public function __construct($id, $title, $value) {
		$this->id = $id;
		$this->title = $title;
		$this->value = $value;
	}

	/**
	 * @param ilPropertyFormGUI $form
	 */

	public function build($form) {
		$title = $this->title;
		$description = null;
		if (is_array($title)) {
			list($title, $description) = $title;
		}

		$cb = new ilNonEditableValueGUI($title, $this->id);
		if ($description) {
			$cb->setInfo($description);
		}
		$cb->setValue($this->value);
		$form->addItem($cb);
	}

	public function save() {
	}
}
