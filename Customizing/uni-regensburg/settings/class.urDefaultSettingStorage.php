<?php

require_once "interface.urSettingStorage.php";

class urDefaultSettingStorage implements urSettingStorage {

	/**
	 * @var string
	 */

	private $id;

	/**
	 * @var mixed
	 */

	private $defaultValue;

	public function __construct($id, $defaultValue) {
		$this->id = $id;
		$this->defaultValue = $defaultValue;
	}

	public function get() {
		global $DIC;
		$ilSetting = $DIC->settings();
		return $ilSetting->get($this->id, $this->defaultValue);
	}

	public function set($value) {
		global $DIC;
		$ilSetting = $DIC->settings();
		$ilSetting->set($this->id, $value);
	}
}
