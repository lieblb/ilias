<?php

class urCheckboxSettingGUI {

	private $id;
	private $title;
	private $storage;

	/**
	 * urCheckboxSettingGUI constructor.
	 * @param string $id
	 * @param string|array $title
	 * @param null $storageClass
	 */

	public function __construct($id, $title, $storageClass = null) {
		if ($storageClass === null) {
			require_once 'class.urDefaultSettingStorage.php';
			$storageClass = urDefaultSettingStorage;
		}

		$this->id = $id;
		$this->title = $title;
		$this->storage = new $storageClass($id, false);
	}

	/**
	 * @param ilPropertyFormGUI $form
	 */

	public function build($form) {
		$title = $this->title;
		$description = null;
		if (is_array($title)) {
			list($title, $description) = $title;
		}

		$cb = new ilCheckboxInputGUI($title, $this->id);
		$cb->setChecked($this->storage->get($this->id));
		if ($description) {
			$cb->setInfo($description);
		}
		$form->addItem($cb);
	}

	public function save() {
		if ($_POST[$this->id]) {
			$this->storage->set(1);
		} else {
			$this->storage->set(0);
		}
	}
}
