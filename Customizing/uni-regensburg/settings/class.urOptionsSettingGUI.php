<?php

class urOptionsSettingGUI {

	private $id;
	private $title;
	private $options;
	private $storage;
	private $defaultValue;

	/**
	 * urOptionsSettingGUI constructor.
	 * @param string $id
	 * @param string|array $title
	 * @param null $storageClass
	 */

	public function __construct($id, $title, $options, $default, $storageClass = null) {
		if ($storageClass === null) {
			require_once 'class.urDefaultSettingStorage.php';
			$storageClass = urDefaultSettingStorage;
		}

		$this->id = $id;
		$this->title = $title;
		$this->options = $options;
		$this->storage = new $storageClass($id, false);
		$this->defaultValue = $default;
	}

	/**
	 * @param ilPropertyFormGUI $form
	 */

	public function build($form) {
		$title = $this->title;
		$description = null;
		if (is_array($title)) {
			list($title, $description) = $title;
		}

		$gui = new ilSelectInputGUI($title, $this->id);
		$gui->setOptions($this->options);
		$value = $this->storage->get($this->id);
		if ($value === false) {
			$value = $this->defaultValue;
		}
		$gui->setValue($value);
		if ($description) {
			$gui->setInfo($description);
		}
		$form->addItem($gui);
	}

	public function save() {
		if ($_POST[$this->id]) {
			$this->storage->set($_POST[$this->id]);
		} else {
			$this->storage->set(0);
		}
	}
}
