<?php

interface urSettingStorage {

	public function get();

	public function set($value);
}
