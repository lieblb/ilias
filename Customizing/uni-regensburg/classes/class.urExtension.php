<?php

class urExtension {

	public function __construct($hooks, $config)
	{
	}

	public static function isEarlyBird()
	{
		return false;
	}

	public function isEnabled()
	{
		return true;
	}

	/**
	 * Handelt es sich beim aktuellen Benutzer um einen ILIAS-Administrator?
	 *
	 * @return bool true, falls ILIAS-Admin
	 */

	protected function isAdministrator()
	{
		global $ilUser, $rbacsystem, $rbacreview;
		if ($ilUser != null && $rbacsystem->checkAccess('write', SYSTEM_FOLDER_ID)) {
			return in_array(SYSTEM_ROLE_ID, $rbacreview->assignedRoles($ilUser->getId()));
		}
		return false;
	}

	protected function getLocalJavaScriptPath($path) {
		$path = @realpath($path);
		if (strpos($path, ILIAS_ABSOLUTE_PATH) === 0) {
			$path = './' . ltrim(substr($path, strlen(ILIAS_ABSOLUTE_PATH)), '/');
		}
		return $path;
	}
}
