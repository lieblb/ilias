<?php

/**
 * Class urAccount
 *
 * Helper for authenticating users and checking password and admin rights (authorization).
 */

class urAccount {
	/**
	 * @var ilObjUser user
	 */
	private $user;

	private function authenticateViaDB($password) {
		require_once 'Services/User/classes/class.ilUserPasswordManager.php';
		return ilUserPasswordManager::getInstance()->verifyPassword($this->user, $password);
	}

	private static function changeKeyCase($a_string) {
		$as_array = array_change_key_case(array($a_string => $a_string));
		foreach($as_array as $key => $string) {
			return $key;
		}
	}

	/**
	 * not yet tested.
	 * @param $password
	 * @return bool
	 */

	private function authenticateViaLDAP($password) {
		$username = $this->user->getLogin();

		try {
			include_once './Services/LDAP/classes/class.ilLDAPServer.php';
			$serverIds = ilLDAPServer::getServerIds();
			if (count($serverIds) < 1) {
				return false;
			}
			$server = ilLDAPServer::getInstanceByServerId($serverIds[0]);
			$server->doConnectionCheck();

			// bind
			include_once './Services/LDAP/classes/class.ilLDAPQuery.php';
			$query = new ilLDAPQuery($server);
			$query->bind(IL_LDAP_BIND_DEFAULT);

			// Read user data, which does ensure a successful authentication.
			$users = $query->fetchUser($username);

			if (!$users) {
				return false;
			}
			if (!array_key_exists(self::changeKeyCase($username), $users)) {
				return false;
			}

			// check group membership
			if (!$query->checkGroupMembership(
				$username,
				$users[self::changeKeyCase($username)])) {
				return false;
			}

			// now bind with login credentials
			$query->bind(IL_LDAP_BIND_AUTH,
				$users[self::changeKeyCase($username)]['dn'], $password);
		}
		catch (ilLDAPQueryException $e) {
			return false;
		}

		return true;
	}

	public function __construct($username) {
		require_once 'Services/Object/classes/class.ilObjectFactory.php';
		require_once 'Services/User/classes/class.ilObjUser.php';
		$user = ilObjectFactory::getInstanceByObjId(ilObjUser::_loginExists($username), false);
		if (!($user instanceof ilObjUser)) {
			$this->user = null;
		} else {
			$this->user = $user;
		}
	}

	public function authenticate($password) {
		if (empty($this->user)) {
			return false;
		}

		if ($this->authenticateViaDB($password)) {
			return true;
		}

		if ($this->authenticateViaLDAP($password)) {
			return true;
		}

		return false;
	}

	public function isAdministrator() {
		if (empty($this->user)) {
			return false;
		}

		global $rbacsystem;
		return $rbacsystem->checkAccessOfUser($this->user->getId(), "write", ROOT_FOLDER_ID, "");
	}
};
