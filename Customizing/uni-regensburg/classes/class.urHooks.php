<?php

use Symfony\Component\Yaml;

require_once 'class.urRegistry.php';

class urHooks {

	private $hooks;

	private $methods;

	/**
	 * @var array
	 */

	private $extensions;

	/**
	 * @var array
	 */

	private $late;

	/**
	 * @param $extension
	 * @param $extensionClass
	 * @throws ReflectionException
	 */

	private function addExtensionInstance($extension, $extensionClass) {
		$name = $extension['name'];

		$r = new ReflectionClass($extensionClass);
		foreach ($r->getMethods(ReflectionMethod::IS_PUBLIC) as $m) {
			$this->hooks->add($name, $m);
			$this->methods->add($name, $m);
		}

		$instance = new $extensionClass($this, $extension);
		$this->extensions[$extensionClass] = $instance;
	}

	/**
	 * @param $extension
	 * @throws ReflectionException
	 */

	private function addExtension($extension) {
		$name = $extension['name'];

		$extensionClass = 'ur' . $name . 'Extension';
		require __DIR__ . '/../extensions/' . $name . '/classes/class.' . $extensionClass . '.php';

		if (call_user_func(array($extensionClass, 'isEarlyBird'))) {
			$this->addExtensionInstance($extension, $extensionClass);
		} else {
			array_push($this->late, array($extension, $extensionClass));
		}
	}

	public function __construct() {
		require_once __DIR__ . '/class.urExtension.php';

		$this->hooks = new urRegistry('iliashook');
		$this->methods = new urRegistry('iliasmethod');

		$this->late = array();
		$this->extensions = array();

		$config = file_get_contents(__DIR__ . '/../config.yml');
		$parsed = Yaml\Yaml::parse($config);

		if ($parsed['enabled']) {
			foreach($parsed['extensions'] as $extension) {
				$this->addExtension($extension);
			}
		}
	}

	public function initLate() {
		foreach ($this->late as $e) {
			$this->addExtensionInstance($e[0], $e[1]);
		}
		$this->late = array();

		$this->executeHooks('globalEntry', array(), 'first');
	}

	public function getExtensions() {
		return $this->extensions;
	}

	/**
	 * @param string $name
	 * @return
	 */

	public function getExtension($name) {
		$extensionClass = 'ur' . $name . 'Extension';
		if (isset($this->extensions[$extensionClass])) {
			return $this->extensions[$extensionClass];
		} else {
			return null;
		}
	}

	public function hasHooks($hookId) {
		return !empty($this->hooks->lookup($hookId, $this));
	}

	/**
	 * @param string $hookId unique id of the hook that should get called
	 * @param array $args
	 * @param string $reduce
	 * @return array|bool|mixed|string
	 */

	public function executeHooks($hookId, $args = array(), $reduce = "array") {
		$results = array();
		foreach ($this->hooks->lookup($hookId, $this) as $hook) {
			array_push($results, call_user_func_array($hook, $args));
		}
		switch ($reduce) {
			case "first":
				return empty($results) ? false : $results[0];
			case "concat":
				return implode('', $results);
			case "or":
				if (empty($results)) {
					return false;
				} else {
					return array_reduce($results, function($a, $b) {
						return $a || $b;
					});
				}
			case "and":
				if (empty($results)) {
					return false;
				} else {
					return array_reduce($results, function ($a, $b) {
						return $a && $b;
					});
				}
			default:
				return $results;
		}
	}

	public function callMethod($instance, $method) {
		if (method_exists($instance, $method)) {
			// since we're not part of $instance, we cannot call protected or private methods.
			// use reflection to call them anyway (thus producing ILIAS's default behaviour).
			$r = new ReflectionMethod(get_class($instance), $method);
			$r->setAccessible(true);
			return $r->invoke($instance);
		}

		$c = get_class($instance);
		while ($c != null) {
			$hooks =  $this->methods->lookup($c . "." . $method, $this);
			if (count($hooks) > 0) {
				return call_user_func_array($hooks[0], array($instance));
			}
			$c = get_parent_class($c);
		}

		throw new Exception("method $method does not exist in class " . get_class($instance));
	}
}
