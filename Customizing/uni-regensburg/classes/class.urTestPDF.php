<?php

require_once './Modules/Test/classes/class.ilTestServiceGUI.php';
require_once './Services/PDFGeneration/classes/factory/class.ilHtmlToPdfTransformerFactory.php';
require_once './Modules/Test/classes/class.ilTestObjectiveOrientedContainer.php';
require_once './Modules/Test/classes/class.ilTestParticipantData.php';
require_once "./Services/Utilities/classes/class.ilUtil.php";


class urTestPDF {
	const DIR_SEP = '/';

	/**
	 * @var ilObjTest
	 */
	private $testObject;

	/**
	 * @var ilObjTestGUI
	 */
	private $gui;

	/**
	 * @var ilHtmlToPdfTransformerFactory
	 */
	private $pdfFactory;

	/**
	 * urTestPDF constructor.
	 * @param ilObjTest $testObject
	 */

	/**
	 * @var ilTestParticipantData
	 */

	private $participantData;

	public function __construct($testObject) {
		$this->testObject = $testObject;

		$serviceGUI = new ilTestServiceGUI($testObject);
		$serviceGUI->setObjectiveOrientedContainer(new ilTestObjectiveOrientedContainer());

		$this->gui = new ilObjTestGUI();
		$this->gui->object = $testObject;

		$this->pdfFactory = new ilHtmlToPdfTransformerFactory();

		global $ilDB, $lng;
		$this->participantData = new ilTestParticipantData($ilDB, $lng);
		$this->participantData->load($testObject->getTestId());
	}

	/**
	 * @param integer $activeId
	 * @param integer $pass
	 * @return bool|string false oder Pfad zum exportierten PDF
	 * @throws ilTemplateException
	 */

	public function exportOne($activeId, $pass) {
		$resultPass = $this->testObject->_getResultPass($activeId);

		if (intval($resultPass) != intval($pass)) {
			ilUtil::sendFailure("Cannot export a PDF for a non-result pass.", true);
			return false;
		}

		$startingTime = $this->testObject->getStartingTimeOfUser($activeId);
		if ($startingTime !== false && $this->testObject->isMaxProcessingTimeReached($startingTime, $activeId)) {
			// ok to export.
		} if ($this->testObject->isTestFinishedToViewResults($activeId, $pass)) {
			// also ok to export.
		} else {
			//ilUtil::sendFailure("Tried to export PDF for unfinished pass.", true);
			//return false;
		}

		$template = $this->gui->createUserResults(true, true, true, array($activeId));

		$html = $template->get();
		$html = ilTestPDFGenerator::preprocessHTML($html);

		$userId = $this->participantData->getUserIdByActiveId($activeId);
		$userData = ilObjUser::_lookupName($userId);

		// this filename will might later be passed into shell_exec, so guard against any kind of injection.
		$filename = "ur_pdf_"
			. strval(intval($this->testObject->getId()))
			. hash('ripemd128', $userData['login'])
			. "_" . strval(intval($activeId))
			. "_" . strval(intval($pass))
			. "_" . time()
			. ".pdf";

		$template->addCss(ilUtil::getStyleSheetLocation("output", "test_print.css", "Modules/Test"), "print");

		require_once './Modules/Test/classes/class.ilTestPDFGenerator.php';
		return ilTestPDFGenerator::generatePDF(
			$template->get(),
			ilTestPDFGenerator::PDF_OUTPUT_DOWNLOAD,
			$filename,
			PDF_USER_RESULT);
	}

	/**
	 * Return the export directory, where zips are placed.
	 *
	 * @return string
	 */
	public function getZipExportDirectory()
	{
		global $ilias;

		$zipPath =
			$ilias->ini_ilias->readVariable('server', 'absolute_path') . self::DIR_SEP
			. $ilias->ini_ilias->readVariable('clients', 'path') . self::DIR_SEP
			. $ilias->client_id . self::DIR_SEP
			. 'assessment' . self::DIR_SEP
			. 'tst_' . $this->testObject->getId() . self::DIR_SEP
			. 'ur_pdf_' . time();

		return $zipPath;
	}

	/**
	 * Exportiert die PDFs aller Teilnehmer im Test.
	 *
	 * @return bool|string
	 * @throws ilException
	 * @throws ilTemplateException
	 */

	public function exportAll()
	{
		$zipOutputPath = $this->getZipExportDirectory();

		if (!file_exists($zipOutputPath)) {
			mkdir($zipOutputPath, 0777, true);
		}

		$participants = $this->testObject->getParticipants();
		foreach($participants as $activeId => $ignored) {
			$resultPass = $this->testObject->_getResultPass($activeId);

			$path = $this->exportOne($activeId, $resultPass);
			if ($path === false) {
				return false;
			}
			$filename = pathinfo($path, PATHINFO_BASENAME);
			if (rename($path, $zipOutputPath . self::DIR_SEP . $filename . ".pdf") !== true) {
				throw new ilException("could not rename file");
			}
		}

		ilUtil::zip($zipOutputPath, $zipOutputPath . ".zip", true);
		ilUtil::delDir($zipOutputPath);

		return $zipOutputPath . ".zip";
	}
}
