<?php

class urRegistry {
	/**
	 * @var array
	 */

	private $entries;

	/**
	 * @var string regexp pattern for annotation.
	 */

	private $preg;

	public function __construct($type) {
		$this->entries = array();
		$this->preg = '#\W@' . $type . '\s+([\w\.]+)#';
	}

	public function add($extensionClass, $m) {
		$doc = $m->getDocComment();
		if (preg_match($this->preg, $doc, $matches) === 1) {

			$hookId = $matches[1];

			if (!isset($this->entries[$hookId])) {
				$this->entries[$hookId] = array();
			}
			array_push($this->entries[$hookId],
				array($extensionClass, $m->getName()));
		}
	}

	public function lookup($hookId, $provider) {
		if (!isset($this->entries[$hookId])) {
			return array();
		} else {
			$res = array();
			foreach ($this->entries[$hookId] as $hook) {
				list($name, $method) = $hook;
				array_push($res, array($provider->getExtension($name), $method));
			}
			return $res;
		}
	}
}
