<?php

/**
 * Prüfen ob die via POST übergebenen credentials einem admin gehören.
 */

header('Content-Type: application/text');

if (!isset($_POST['username']) || !isset($_POST['password'])) {
	echo 'ERROR';
	die;
}

try {
	chdir("../../..");

	require_once 'Services/Context/classes/class.ilContext.php';
	ilContext::init(ilContext::CONTEXT_SOAP_NO_AUTH);

	require_once("./Services/Init/classes/class.ilInitialisation.php");
	ilInitialisation::initILIAS();

	$account = new Account($_POST['username']);

	if ($account->authenticate($_POST['password']) && $account->isAdministrator()) {
		echo "AUTHENTICATED_ADMIN";
	} else {
		echo "FAILED";
	}

} catch(\Exception $e) {
	echo "ERROR";
}
