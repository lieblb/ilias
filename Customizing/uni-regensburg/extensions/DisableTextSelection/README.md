Verhindern des versehentlichen Löschens von Textblöcken bei Freitextfragen.

Sinnvoller Workaround gegen Verlust von Daten falls keine Versionierung aktiviert ist.
