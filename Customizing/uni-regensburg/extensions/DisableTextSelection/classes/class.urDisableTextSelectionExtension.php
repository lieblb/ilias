<?php

class urDisableTextSelectionExtension extends urExtension {

	public function isEnabled() {
		global $ilSetting;
		return $ilSetting->get("ur_allow_kiosk_text_selection") != 0;
	}

	public function createSettingGUI() {
		require_once __DIR__ . '/../../../settings/class.urCheckboxSettingGUI.php';

		return array(
			"Antwortversionen" => array(
				new urCheckboxSettingGUI(
					"ur_allow_kiosk_text_selection",
					array(
						"Löschen von Textblöcken erlauben",
						"Erlaubt im Kioskmodus die Selektion von Textblöcken. " .
							"Nur mit Antwortversionen sicher, da sonst Gefahr von Datenverlusten.")
				)
			)
		);
	}

	/**
	 * @iliashook ilTestPlayerAbstractGUI.prepareTestPage
	 * @param ilTemplate $tpl
	 * @param bool inKioskMode
	 */

	public function configureTestPlayerTemplate($tpl, $inKioskMode) {
		if ($inKioskMode && $this->isEnabled()) {
			$tpl->addJavascript($this->getLocalJavaScriptPath(__DIR__ . '../js/urDisableTextSelection.js'), false);
		}
	}
}
