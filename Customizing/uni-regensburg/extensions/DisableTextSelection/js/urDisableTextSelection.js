document.addEventListener('keydown',function(e) {
    var focus = document.activeElement;
    if (focus && focus.selectionStart < focus.selectionEnd) {
        var allowed = {
            "ArrowLeft": true,
            "ArrowRight": true,
            "ArrowUp": true,
            "ArrowDown": true,
            "Tab": true
        };
        if (!allowed[e.key]) {
            e.preventDefault();
        }
    }
}, true);

if (typeof tinyMCE != 'undefined') {
    // tinymce 4: tinyMCE.on('addeditor', function())
    tinyMCE.onAddEditor.add(function(mgr, ed) {
        // console.log(ed);
        ed.onKeyPress.add(function(ed, k) {
            ed.selection.collapse();
        });
        ed.onKeyDown.add(function(ed, k) {
            ed.selection.collapse();
        });
    });
}
