<?php

class urDisableMailsExtension extends urExtension {

	/**
	 * @iliashook ilMail.sendMail
	 * @param mixed ...$args
	 * @return array
	 */

	public function sendMail(...$args)
	{
		return array(array('Sending mails is not allowed on this system.', ''));
	}
}
