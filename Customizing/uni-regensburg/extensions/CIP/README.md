UR-spezifische CIP-Raumsteuerung.

Kümmert sich um den automatischen Boot von Rechnern in CIP-Pools ins E-Prüfungslinux
sowie um die Konfiguration von derart gebooteten Rechnern.
