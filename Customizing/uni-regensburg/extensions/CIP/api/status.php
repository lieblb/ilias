<?php

// returns which status a CIP room is currently in, i.e. if it's in assessment mode.

$config = file_get_contents(__DIR__ . '/../../../config.yml');

chdir("../../../../..");

require_once 'libs/composer/vendor/autoload.php';

use Symfony\Component\Yaml;

$parsed = Yaml\Yaml::parse($config);

if (!isset($_GET['key']) || $_GET['key'] != $parsed['extensions']['CIP']['secrets']['query'] ||
	!isset($_GET['raum']) || $_GET['raum'] != 'bib3') {
	echo 'ERROR';
	die;
}

try {
	require_once 'Services/Context/classes/class.ilContext.php';
	ilContext::init(ilContext::CONTEXT_SOAP_NO_AUTH);

	require_once("./Services/Init/classes/class.ilInitialisation.php");
	ilInitialisation::initILIAS();

	global $DIC;
	$ilSetting = $DIC->settings();

	if ($ilSetting->get("ur_seb_" . $_GET['raum']) != 'off') {
		echo "EPRUEFUNG";
	} else {
		echo "CIP";
	}
} catch(\Exception $e) {
	echo "ERROR";
}
