<?php

/*
 * generates shell script code embedded into the linux boot phase on CIP clients.
 */

require_once __DIR__ . '/../../../../../libs/composer/vendor/autoload.php';

use Symfony\Component\Yaml;

$config = file_get_contents(__DIR__ . '/../../../config.yml');
$parsed = Yaml\Yaml::parse($config);

if (isset($_POST['secret']) && $_POST['secret'] == $parsed['extensions']['CIP']['secrets']['boot']) {
	echo <<<EOT
#!/bin/bash
echo "linuxboot loaded.";
EOT;
} else {
	http_response_code(403);
	die();
}
