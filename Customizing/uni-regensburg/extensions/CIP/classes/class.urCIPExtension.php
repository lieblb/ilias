<?php

class urCIPExtension extends urExtension {

	public function createSettingGUI()
	{
		require_once __DIR__ . '/../../../settings/class.urCheckboxSettingGUI.php';
		require_once __DIR__ . '/../../../settings/class.urOptionsSettingGUI.php';

		return array(
			"Raumsteuerung BIB3" => array(
				new urOptionsSettingGUI(
					"ur_seb_bib3",
					array("SEB", "Definiert ob und wie der SEB gebootet wird."),
					array(
						'off' => "Ausgeschaltet",
						'exam' => "SEB mit E-Prüfungssystem",
						'oup' => "SEB mit OUP Einstufungstest"
					),
					'off'
				)
			)
		);
	}
}
