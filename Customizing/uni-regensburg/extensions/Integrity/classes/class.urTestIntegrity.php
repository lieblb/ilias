<?php

// we cannot create a constraint on tst_solutions(active_fi, question_fi, pass, value1), as (1) not all question
// types might be unique in this tuple (most are), (2) value1 is a blob and cannot be used in a constraint in
// its full length.

class urTestIntegrity {
	/**
	 * @var ilObjTest $testObj
	 */
	private $testObj;

	public function __construct($testObj) {
		$this->testObj = $testObj;
	}

	/*
	 * Liefert für eine geg. Frage-ID den Fragetyp:
	 *
	+------------------+-----------------------+--------+-------------+
	| question_type_id | type_tag              | plugin | plugin_name |
	+------------------+-----------------------+--------+-------------+
	|                1 | assSingleChoice       |      0 |             |
	|                2 | assMultipleChoice     |      0 |             |
	|                3 | assClozeTest          |      0 |             |
	|                4 | assMatchingQuestion   |      0 |             |
	|                5 | assOrderingQuestion   |      0 |             |
	|                6 | assImagemapQuestion   |      0 |             |
	|                7 | assJavaApplet         |      0 |             |
	|                8 | assTextQuestion       |      0 |             |
	|                9 | assNumeric            |      0 |             |
	|               10 | assTextSubset         |      0 |             |
	|               12 | assFlashQuestion      |      0 |             |
	|               13 | assOrderingHorizontal |      0 |             |
	|               14 | assFileUpload         |      0 |             |
	|               15 | assErrorText          |      0 |             |
	|               16 | assFormulaQuestion    |      0 |             |
	|               17 | assKprimChoice        |      0 |             |
	|               18 | assLongMenu           |      0 |             |
	+------------------+-----------------------+--------+-------------+

	/**
	 * @param var $questionTypeTag Name des Fragetyps
	 * @return ilDBStatement|ilPDOStatement
	 */

	private function getSolutions($questionTypeTag) {
		global $ilDB;

		$res = $ilDB->queryF(<<<EOT
SELECT active_fi, question_fi, pass, value1 FROM tst_solutions s
INNER JOIN qpl_questions q on q.question_id = s.question_fi
INNER JOIN qpl_qst_type t ON t.question_type_id = q.question_type_fi
INNER JOIN tst_active a on s.active_fi = a.active_id
WHERE a.test_fi = (SELECT test_id FROM tst_tests WHERE obj_fi = %s) AND t.type_tag = %s AND s.authorized > 0
EOT
			, array("integer", "string"), array($this->testObj->getId(), $questionTypeTag));

		return $res;
	}

	/**
	 * @param $resultSet
	 * @param $fields
	 * @param callable $createVerifier
	 * @return bool
	 */

	private function verifyUnique($resultSet, $fields, $createVerifier = null) {
		$seen = array();
		$verifiers = array();

		global $ilDB;
		while ($row = $ilDB->fetchAssoc($resultSet)) {
			if ($createVerifier !== null) {
				$questionId = $row["question_fi"];
				if (!isset($verifiers[$questionId])) {
					$verifiers[$questionId] = $createVerifier($questionId);
				}
				if (!$verifiers[$questionId]($row)) {
					return false;
				}
			}

			$data = array();
			foreach ($fields as $field) {
				array_push($data, strval($row[$field]));
			}

			$key = implode("/", $data);
			if (isset($seen[$key])) {
				return false;
			}
			$seen[$key] = true;
		}
		return true;
	}

	private function verifySingleChoiceSolutionsIntegrity() {
		return $this->verifyUnique($this->getSolutions("assSingleChoice"),
			array("active_fi", "question_fi", "pass"),
			function($questionId) {
				global $ilDB;
				$res = $ilDB->queryF("SELECT MAX(aorder) FROM qpl_a_sc WHERE question_fi = %s",
					array("integer"), array($questionId));
				$max = intval(array_pop($ilDB->fetchAssoc($res)));
				return function($row) use ($max) {
					$value1 = $row["value1"];
					return intval($value1) >= 0 && intval($value1) <= $max;
				};
			});
	}

	private function verifyMultipleChoiceSolutionsIntegrity() {
		return $this->verifyUnique($this->getSolutions("assMultipleChoice"),
			array("active_fi", "question_fi", "pass", "value1"),
			function($questionId) {
				global $ilDB;
				$res = $ilDB->queryF("SELECT MAX(aorder) FROM qpl_a_mc WHERE question_fi = %s",
					array("integer"), array($questionId));
				$max = intval(array_pop($ilDB->fetchAssoc($res)));
				return function($row) use ($max) {
					$value1 = $row["value1"];
					return intval($value1) >= 0 && intval($value1) <= $max;
				};
			});
	}

	private function verifyTextQuestionSolutionsIntegrity() {
		return $this->verifyUnique($this->getSolutions("assTextQuestion"),
			array("active_fi", "question_fi", "pass"));
	}

	private function verifyResultIntegrity() {
		global $ilDB;

		$res = $ilDB->queryF(<<<EOT
select MAX(t.x) from (
select count(*) as x from tst_test_result r
	inner join tst_active a on r.active_fi=a.active_id where a.test_fi=(SELECT test_id FROM tst_tests WHERE obj_fi=%s)
	group by active_fi, question_fi, pass) as t
EOT
			, array("integer"), array($this->testObj->getId()));

		$duplicates = intval(array_pop($ilDB->fetchAssoc($res)));
		return $duplicates <= 1;
	}

	public function isGood() {
		if (!$this->verifyResultIntegrity()) {
			return false;
		}

		if (!$this->verifySingleChoiceSolutionsIntegrity()) {
			return false;
		}

		if (!$this->verifyMultipleChoiceSolutionsIntegrity()) {
			return false;
		}

		if (!$this->verifyTextQuestionSolutionsIntegrity()) {
			return false;
		}

		return true;
	}
}