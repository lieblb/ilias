<?php

require_once __DIR__ . '/../../../settings/interface.urSettingStorage.php';

class urResultConstraint implements urSettingStorage {

	/**
	 * @var string
	 */

	private $id;

	public function __construct($id, $defaultValue) {
		$this->id = $id;
	}

	public function get() {
		global $ilDB;
		return boolval($ilDB->uniqueConstraintExists(
			'tst_test_result', array('active_fi', 'question_fi', 'pass')));
	}

	public function set($enable) {
		if (boolval($enable) == $this->get()) {
			return;
		}
		global $ilDB;
		if ($enable) {
			$ilDB->addUniqueConstraint('tst_test_result', array('active_fi', 'question_fi', 'pass'), 'ur1');
		} else {
			$ilDB->dropUniqueConstraint('tst_test_result', 'ur1');
		}
	}
}
