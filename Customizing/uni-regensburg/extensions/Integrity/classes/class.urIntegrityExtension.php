<?php

class urIntegrityExtension extends urExtension {

	private $hooks;

	public function __construct($hooks, $config)
	{
		$this->hooks = $hooks;
	}

	public function createSettingGUI() {
		require_once __DIR__ . '/../../../settings/class.urCheckboxSettingGUI.php';
		require_once __DIR__ . '/../../../settings/class.urNonEditableSettingGUI.php';

		$assessmentSetting = new ilSetting('assessment');
		$assessmentLocking = $assessmentSetting->get('ass_process_lock_mode', 'none');

		require_once 'class.urResultConstraint.php';

		return array(
			"Datenintegrität" => array(
				new urNonEditableSettingGUI(
					"ur_check_test_locking",
					array(
						"Locking für Testmodul",
						"Methode: " . $assessmentLocking
					),
					$assessmentLocking == "none" ? "nicht aktiviert (für Datenverluste anfällig)" : "aktiviert"
				),
				new urCheckboxSettingGUI(
					"ur_constraint_test_result",
					array(
						'Schutz vor doppelter Punktzählung',
						'Ist ein DB-Constraint zum Verhindern doppelter Einträge auf tst_test_result installiert?'
					),
					urResultConstraint
				)
			)
		);
	}

	/**
	 * @iliashook ilTestExportGUI.listExportFiles
	 * @param ilToolbarGUI $toolbar
	 */

	public function addExportButtons($toolbar)
	{
		$toolbar->addFormButton("Integrität prüfen", "verifyIntegrity");
	}

	/**
	 * @iliashook ilExportGUI.executeCommand
	 * @param string $cmd
	 * @param ilTestExportGUI $gui
	 */

	public function executeExportCommand($cmd, $gui)
	{
		switch ($cmd) {
			case "verifyIntegrity": {
				$this->verifyTestIntegrity($gui->obj);
				global $ilCtrl;
				$ilCtrl->redirect($gui, 'listExportFiles');
				return true;
			} break;
		}

		return false;
	}

	/**
	 * Prüft, ob die im Test abgespeicherten Antworten grob dem Datenschema entsprechen.
	 *Ø
	 * @param ilObjTest $testObj
	 */

	public function verifyTestIntegrity($testObj)
	{
		if (!$this->isAdministrator()) {
			return; // Für Nicht-Adminstratoren werden in keinem Fall UR-interne Warnhinweise angezeigt.
		}

		require_once 'class.urTestIntegrity.php';
		$integrity = new urTestIntegrity($testObj);

		if ($integrity->isGood() !== true) {
			ilUtil::sendFailure("Datenintegrität und/oder Testergebnisse des Tests scheinen beschädigt zu sein.", true);
			return false;
		} else {
			ilUtil::sendSuccess("Grundlegende Datenintegrität OK.", true);
			return true;
		}
	}
}
