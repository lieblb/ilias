<?php

class urErrorLogsExtension extends urExtension {

	public function __construct($hooks, $config)
	{
		$hooks->getExtension('Translations')->addTranslation("urErrorLogs", "Fehlerlogs");
	}

	/**
	 * @iliashook ilObjSystemFolderGUI.getAdminTabs
	 * @param ilObjSystemFolderGUI $gui
	 * @param ilTabsGUI tabs
	 * @param ilCtrl ctrl
	 */

	public function addSystemFolderTabs($gui, $tabs, $ctrl)
	{
		global $rbacsystem;

		if ($rbacsystem->checkAccess("write", $gui->object->getRefId())) {
			$tabs->addTarget("urErrorLogs",
				$ctrl->getLinkTarget($gui, "urErrorLogs"), "urErrorLogs", get_class($gui));
		}
	}

	/**
	 *
	 * @iliashook ilObjSystemFolderGUI.executeCommand
	 * @param ilObjSystemFolderGUI $gui
	 * @param array $data
	 * @param string $cmd
	 * @return bool
	 */

	public function executeSystemFolderCommand($gui, $data, $cmd) {
		switch ($cmd) {
			case "urErrorLogs":
				$rbacsystem = $data->rbacsystem;
				$lng = $data->lng;
				$ilCtrl = $data->ctrl;
				$ilSetting = $data->settings;
				$tpl = $data->tpl;
				$ilErr = $data->error;

				if (!$rbacsystem->checkAccess("visible,read", $data->object->getRefId())) {
					$ilErr->raiseError($data->lng->txt("permission_denied"), $ilErr->MESSAGE);
				}

				$ilTabs = $data->tabs;
				$ilTabs->activateTab("urErrorLogs");

				$tpl->setContent($this->getErrorLogsHTML());
				return true;
		}

		return false;
	}

	private function getErrorLogsHTML() {
		$fi = new FilesystemIterator(ILIAS_LOG_DIR, FilesystemIterator::SKIP_DOTS);

		$entries = array();
		$entries[] = "<h3>" . strval(iterator_count($fi)) . " Fehlerlogs.</h3>";

		$files = array();
		foreach ($fi as $fileinfo) {
			$files[$fileinfo->getMTime()][] = $fileinfo->getFilename();
		}

		krsort($files);

		foreach ($files as $key => $filenames) {
			foreach ($filenames as $filename) {
				if ($filename != "ilias.log") {
					$entries[] = "<h3>" . date('Y-m-d H:i:s', $key) . " " . $filename . "</h3>" .
						"<pre>" . @file_get_contents(ILIAS_LOG_DIR . "/" . $filename) . "</pre>";
				}
			}

			if (count($entries) > 5) {
				break;
			}
		}

		return implode("<br>", $entries);
	}
}
