<?php

class urVersions {
	/**
	 * Install the DB table used for storing answer versions.
	 */

	public static function install() {
		global $ilDB;
		if(!$ilDB->tableExists('ur_answer_versions'))
		{
			$fields = array(
				'version_id' => array('type' => 'integer', 'length' => 4, 'notnull' => true),
				'active_fi' => array('type' => 'integer', 'length' => 4, 'notnull' => true),
				'pass' => array('type' => 'integer', 'length' => 4, 'notnull' => true),
				'question_fi' => array('type' => 'integer', 'length' => 4, 'notnull' => true),
				'cells' => array('type' => 'blob', 'notnull' => true),
				'tstamp' => array('type' => 'integer', 'length' => 4, 'notnull' => true)
			);
			$ilDB->createTable('ur_answer_versions', $fields);
			$ilDB->addPrimaryKey('ur_answer_versions', array('version_id'));
			$ilDB->createSequence('ur_answer_versions');
			$ilDB->addIndex('ur_answer_versions', array('active_fi', 'pass', 'question_fi', 'version_id'), 'i1');
		}
	}

	public static function getStorageInfo() {
		global $ilDB;
		if ($ilDB->tableExists('ur_answer_versions')) {
			$q = "SELECT (data_length + index_length) AS nbytes FROM information_schema.TABLES WHERE table_name='ur_answer_versions';";
			$result = $ilDB->fetchAssoc($ilDB->query($q));
			$nbytes = $result["nbytes"];

			$q = "SELECT COUNT(version_id) as nrecords FROM ur_answer_versions";
			$result = $ilDB->fetchAssoc($ilDB->query($q));
			$nrecords = $result["nrecords"];

			return array($nrecords, $nbytes);
		} else {
			return 0;
		}
	}

	public static function erase() {
		global $ilDB;
		if ($ilDB->tableExists('ur_answer_versions')) {
			$olderThan = 3600 * 8; // 8 hours
			$ilDB->manipulate(
				"DELETE FROM ur_answer_versions WHERE tstamp < " . $ilDB->quote(time() - $olderThan, "integer"));
		}
	}

	public static function isEnabled() {
		global $ilSetting, $ilDB;
		return $ilSetting->get("ur_enable_answer_versions") && $ilDB->tableExists('ur_answer_versions');
	}
}
