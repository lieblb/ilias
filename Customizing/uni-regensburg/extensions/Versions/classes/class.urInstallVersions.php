<?php

require_once __DIR__ . '/../../../settings/class.urDefaultSettingStorage.php';

class urInstallVersions extends urDefaultSettingStorage {

	public function set($value) {
		if ($value) {
			require_once 'class.urVersions.php';
			urVersions::install();
		}
		parent::set($value);
	}
}
