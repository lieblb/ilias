<?php

require_once 'class.urVersions.php';

class urVersionsExtension extends urExtension {

	private $hooks;
	private $config;
	private $questions;

	public function __construct($hooks, $config) {
		$this->hooks = $hooks;
		$this->config = $config;

		$this->questions = array();
		if (isset($config['questions'])) {
			foreach ($config['questions'] as $q) {
				$this->questions[$q] = true;
			}
		}
	}

	private function isVersionedQuestionType($type) {
		// saving versions produce additional server load. only do this for
		// question types where states are not trivial.
		return isset($this->questions[$type]);
	}

	public function isEnabled() {
		return urVersions::isEnabled();
	}

	public function createSettingGUI() {
		require_once __DIR__ . '/../../../settings/class.urCheckboxSettingGUI.php';

		require_once 'class.urInstallVersions.php';
		require_once 'class.urDeleteVersions.php';

		list($nrecords, $nbytes) = urVersions::getStorageInfo();

		return array(
			"Antwortversionen" => array(
				new urCheckboxSettingGUI(
					"ur_enable_answer_versions",
					array(
						"Antwortversionen aktivieren",
						"Aktiviert das Speichern zusätzlicher Versionen gegebenener Antworten zur zusätzlichen Absicherung gegen Datenverlust."
					),
					urInstallVersions
				),
				new urCheckboxSettingGUI(
					"ur_delete_solution_versions",
					array(
						"Antwortversionen löschen",
						sprintf("Momentan %d Einträge, %.1f MB", $nrecords, ($nbytes / (1024 * 1024)))
					),
					urDeleteVersions
				)
			)
		);
	}

	private function isSameSolution($a, $b) {
		if ($a == null || $b == null) {
			return false;
		}

		$a = json_decode($a, true);
		$b = json_decode($b, true);

		if (!is_array($a) || !is_array($b)) {
			return false;
		}

		$z = count($a);
		if ($z != count($b)) {
			return false;
		}
		for ($i = 0; $i < $z; $i++) {
			foreach (array('solution_id', 'tstamp', 'authorized') as $c) {
				if (isset($a[$i][$c])) {
					unset($a[$i][$c]);
				}
				if (isset($b[$i][$c])) {
					unset($b[$i][$c]);
				}
			}
			if (serialize($a[$i]) != serialize($b[$i])) {
				return false;
			}
		}

		return true;
	}

	/**
	 * Wird beim Löschen von Teilnehmerergebnissen aufgerufen.
	 *
	 * @iliashook ilObjTest.removeTestResultsByActiveIds
	 */

	public function removeTestResultsByActiveIds($activeIds) {
		global $ilDB;
		if ($ilDB->tableExists('ur_answer_versions')) {
			$IN_activeIds = $ilDB->in('active_fi', $activeIds, false, 'integer');
			$ilDB->manipulate("DELETE FROM ur_answer_versions WHERE $IN_activeIds");
		}
	}

	/**
	 *
	 * Wird beim Speichern der Antwort eines Teilnehmers aufgerufen.
	 *
	 * @iliashook assQuestion.persistWorkingState
	 * @param assQuestion $question
	 * @param integer $activeId
	 * @param integer $pass
	 */

	public function persistQuestionWorkingState($question, $activeId, $pass)
	{
		global $ilSetting;
		if ($ilSetting->get("ur_enable_answer_versions")) {
			// saving versions produces load. only do this for
			// complex questions where states are not trivial.

			if (!$this->isVersionedQuestionType($question->getQuestionType())) {
				return; // don't save versions
			}

			global $ilDB;
			if ($ilDB->tableExists('ur_answer_versions')) {
				$cells = json_encode($question->getUserSolutionPreferingIntermediate($activeId, $pass));

				if (isset($_POST['formtimestamp'])) {
					$tstamp = intval($_POST['formtimestamp']);
				} else {
					throw new \Exception("no formtimestamp found in POST data");
				}

				$storedCells = null;
				$storedTStamp = null;

				$query = 'SELECT tstamp, cells FROM ur_answer_versions ' .
					'WHERE active_fi = ' . $ilDB->quote($activeId, 'integer') . ' ' .
					'AND pass = ' . $ilDB->quote($pass, 'integer') . ' ' .
					'AND question_fi = ' . $ilDB->quote($question->id, 'integer') . ' ' .
					'ORDER BY version_id DESC LIMIT 1';
				if ($row = $ilDB->fetchAssoc($ilDB->query($query))) {
					$storedCells = $row['cells'];
					$storedTStamp = intval($row['tstamp']);

					if ($tstamp < $storedTStamp) {
						// something is wrong here. this is old form data arriving out of order.
						throw new \Exception("detected out-of-order submit with stale formtimestamp");
					}
				}

				if (!$this->isSameSolution($cells, $storedCells)) { // don't duplicate solutions
					if (false && $tstamp == $storedTStamp) {
						// different data with same timestamp; this should not happen theoretically,
						// but it seems to happen on test finish. need to investigate further.
						throw new \Exception("detected illegal submit with duplicate formtimestamp");
					}

					$ilDB->manipulateF(
						"INSERT INTO ur_answer_versions(version_id, active_fi, pass, question_fi, cells, tstamp) VALUES (%s,%s,%s,%s,%s,%s)",
						array("integer", "integer", "integer", "integer", "blob", "integer"),
						array($ilDB->nextId("ur_answer_versions"), $activeId, $pass, $question->id, $cells, $tstamp));
				}
			}
		}
	}

	/**
	 *
	 * Erlaubt das Einfügen einer Lösung, die nicht der letzten aktuellen Benutzerlösung entspricht. Dies
	 * darf offensichtlich nur unter einer sicheren, mehrfach gesicherten Prüfung passieren.
	 *
	 * Die Funktion muss unter allen Umständen, die nicht dem Versionierungs-Rollback dienen, ein Array
	 * liefern, das als ersten Parameter "false" enthält, und somit kein Überschreiben anzeigt.
	 *
	 * @iliashook assQuestion.overrideQuestionUserSolution
	 * @param assQuestion $question
	 * @param integer $activeId
	 * @param integer $pass
	 * @return array (bool override, array solution), wo "override" anzeigt, ob ein Überschreiben stattfinden
	 * soll, und "solution" die Lösung ist, durch die die aktuelle Lösung ersetzt werden soll.
	 */

	public function overrideQuestionUserSolution($question, $activeId, $pass)
	{
		if (isset($_SESSION["urInTestAdministration"]) && $_SESSION["urInTestAdministration"] == "enabled") {
			if (isset($_SESSION["urPreviousAnswerVersion"]) && intval($_SESSION["urPreviousAnswerVersion"]) >= 1) {
				require_once __DIR__ . '/../classes/class.urVersions.php';
				if (urVersions::isEnabled()) {
					return array(true, $this->getVersionedSolution(
						$question->getId(), $activeId, $pass, intval($_SESSION["urPreviousAnswerVersion"])));
				}
			}
		}

		return false;
	}

	/**
	 * @iliashook ilTestQuestionNavigationGUI.getActionsHTML
	 * @param ilTestQuestionNavigationGUI $navigationGUI
	 * @param ilGroupedListGUI $actions
	 */

	public function addNavigationMenuEntry($navigationGUI, $actions) {
		if ($navigationGUI->urInTestAdministrationLinkTarget) {
			$actions->addSeparator();
			$actions->addEntry("Versionsverwaltung", $navigationGUI->urInTestAdministrationLinkTarget, '', '',
				'ilTestQuestionAction', 'ur_tst_in_test_administration');
		}
	}

	/**
	 *
	 * Toolbar, die bei Testeinsicht via Test > Teilnehmer > Detaillierte Ergebnisse zu erreichen ist. Wir blenden
	 * hier einen zusätzlichen Button ein für die Antworthistorie.
     *
	 * @iliashook ilTestResultsToolbarGUI.build
	 * @param ilToolbarGUI $toolbar
	 * @return bool
	 */

	public function extendTestResultsToolbar($toolbar)
	{
		if (urVersions::isEnabled()) {
			if (!isset($_GET["urShowVersions"]) || $_GET["urShowVersions"] == 0) {
				$toolbar->addSeparator();

				require_once 'Services/UIComponent/Button/classes/class.ilLinkButton.php';
				$link = ilLinkButton::getInstance(); // always returns a new instance
				$link->setUrl($_SERVER[REQUEST_URI] . "&urShowVersions=1");
				$link->setCaption("Zeige Versionen", false);
				$link->setOmitPreventDoubleSubmission(true);

				$toolbar->addButtonInstance($link);
			}
		}
	}

	/**
	 * @iliashook ilTestPlayerAbstractGUI.showQuestionEditable
	 * @param ilTestPlayerAbstractGUI $player
	 * @param ilTestQuestionNavigationGUI $questionNavigationGUI
	 * @param integer $questionId Id der angezeigten Frage
	 */

	public function showQuestionEditable($player, $questionNavigationGUI, $questionId)
	{
		if (urVersions::isEnabled()) {
			if ($this->isVersionedQuestionType(
				assQuestion::getQuestionTypeFromDb($questionId))) {
				$questionNavigationGUI->urInTestAdministrationLinkTarget =
					$player->ctrl->getLinkTarget($player, "urInTestAdministration");
			}
		}
	}

	/**
	 * @iliashook ilTestServiceGUI.getPassListOfAnswers
	 * @param integer $questionId Id der Frage, zu der eine Antwortansicht ausgegeben wird
	 * @param integer $activeId
	 * @param integer $pass
	 * @return string HTML, das an die Antwortansicht für eine Frage angehängt werden soll
	 */

	public function additionalPassListOfAnswersHtml($questionId, $activeId, $pass)
	{
		if (!$this->isAdministrator()) {
			return "";
		}

		$answer_history = "";
		if (isset($_GET['urShowVersions']) && $_GET['urShowVersions']) {
			global $ilSetting, $ilDB;
			if ($ilSetting->get("ur_enable_answer_versions") && $ilDB->tableExists('ur_answer_versions')) {
				$answer_history .= "<br>";

				$set = $ilDB->query("SELECT * FROM ur_answer_versions" .
					" WHERE active_fi=" . $ilDB->quote($activeId, "integer") .
					" AND pass=" . $ilDB->quote($pass, "integer") .
					" AND question_fi=" . $ilDB->quote($questionId, "integer") .
					" ORDER BY version_id DESC");

				$maxVersions = 5;

				$index = 1;
				$entries = array();
				while ($rec = $ilDB->fetchAssoc($set)) {
					$solutions = json_decode($rec["cells"], true);

					$versionHtml = '<div id="ur_versions_no_' . strval($index) . '" style="margin-left:5em; display: none;">';
					$versionHtml .= "<p><h3>Frühere Fassung " . strval($index) . " [" . date("d.n.Y, H:i:s", $rec["tstamp"]) . "]</h3>";

					foreach ($solutions as $solution) {
						if (!empty($solution["value2"])) {
							$versionHtml .= "<table>";
							$versionHtml .= '<tr>';
							$versionHtml .= '<td style="padding:0 1em 0 1em;">';
							$versionHtml .= $solution["value1"];
							$versionHtml .= "</td>";
							$versionHtml .= '<td style="padding:0 1em 0 1em;">';
							$versionHtml .= $solution["value2"];
							$versionHtml .= "</td>";
							$versionHtml .= "</tr>";
							$versionHtml .= "</table>";
						} else {
							$versionHtml .= '<div class="ilc_qanswer_Answer solutionbox"><p>';
							$versionHtml .= $solution["value1"];
							$versionHtml .= '</p></div>';
						}
					}

					$versionHtml .= '</p></div>';
					array_push($entries, $versionHtml);

					if (count($entries) >= $maxVersions) {
						break;
					}

					$index += 1;
				}

				if (!empty($entries)) {
					$answer_history .= '<div style="border-color:#b1e8ee; border-style: solid; border-width: 0.5em; padding: 1em;">';
					$answer_history .= '<h3>Antwortversionen</h3>';
					$answer_history .= '<input type="range" min="1" max="1" value="1" id="ur_versions_slider">';

					$numEntries = count($entries);
					$answer_history .= <<<JS
<script>
	$(function() {
	    function showVersion(which) {
			for (var i = 1; i <= $numEntries; i++) {
				$("#ur_versions_no_" + i.toString()).css("display", "none");				
			}
			$("#ur_versions_no_" + which.toString()).css("display", "block");
	    }

		$("#ur_versions_slider").attr("max", $numEntries);
		$("#ur_versions_slider").val($numEntries);
		showVersion($("#ur_versions_slider").val());
		
		$("#ur_versions_slider").change(function() {
			showVersion($(this).val());
		});
	});
</script>
JS;

					$answer_history .= implode("", array_reverse($entries));

					$answer_history .= '</div>';
				} else {
					$answer_history .= "Keine Versionen vorhanden.";
				}


				$answer_history .= '<div style="padding-bottom:2em;"></div>';
			}
		}
		return $answer_history;
	}

	/**
	 * @iliashook assQuestion.outQuestionForTest
	 * @param addQuestionGUI $gui
	 * @return string An das Frage-HTML zusätzlich anzuhängendes HTML
	 */

	public function appendInTestQuestionHtml($active_id, $pass, $question, $gui) {

		if (!$this->isVersionedQuestionType($question->getQuestionType())) {
			unset($_SESSION["urInTestAdministration"]);
		    return;
        }

		if (isset($_SESSION["urInTestAdministration"])) {

			if ($_SESSION["urInTestAdministration"] == "requested") {
				$tpl = new ilTemplate(__DIR__ . "/../templates/tpl.enter_administration.html", true, false);
				return $tpl->get();
			}

			if ($_SESSION["urInTestAdministration"] == "enabled") {
				$previousAnswerVersion = strval(intval($_SESSION["urPreviousAnswerVersion"]));

				$record = $this->getVersionRecord($active_id, $pass, $question, intval($previousAnswerVersion));

				$tpl = new ilTemplate(__DIR__ . "/../templates/tpl.inspect_versions.html", true, false);
				$tpl->setCurrentBlock("inspect");
				if ($record) {
					$date = date('d.m.Y H:i:s', $record['tstamp']);

					$tpl->setVariable("PREVIOUS_ANSWER_VERSION", $previousAnswerVersion);
					$tpl->setVariable("PREVIOUS_ANSWER_TIMESTAMP", $date);
				} else {
					$tpl->setVariable("PREVIOUS_ANSWER_VERSION", 'n/a');
					$tpl->setVariable("PREVIOUS_ANSWER_TIMESTAMP",'n/a');
				}
				//$tpl->parseCurrentBlock();
				return $tpl->get();
			}
		}

		return '';
	}

	/**
	 * @param integer $questionId
	 * @param integer $activeId
	 * @param integer $pass
	 * @param integer $reverseId Mit 1 beginnende ID der vergangenen Version (höhere Versionen sind älter)
	 * @return bool|mixed
	 */

	private function getVersionedSolution($questionId, $activeId, $pass, $reverseId) {
		if (urVersions::isEnabled()) {
			global $ilDB;
			$set = $ilDB->query("SELECT * FROM ur_answer_versions" .
				" WHERE active_fi=" . $ilDB->quote($activeId, "integer") .
				" AND pass=" . $ilDB->quote($pass, "integer") .
				" AND question_fi=" . $ilDB->quote($questionId, "integer") .
				" ORDER BY tstamp DESC, version_id DESC" .
				" LIMIT " . $ilDB->quote($reverseId, "integer"));

			$index = 1;
			while ($rec = $ilDB->fetchAssoc($set)) {
				if ($index == $reverseId) {
					$solution = json_decode($rec["cells"], true);
					return $solution;
				}
				$index += 1;
			}

			return array();
		}

		return array();
	}

	private function getVersionRecord($activeId, $pass, $question, $version) {
		global $ilDB;

		$query = 'SELECT * FROM ur_answer_versions ' .
			'WHERE active_fi = ' . $ilDB->quote($activeId, 'integer') . ' ' .
			'AND pass = ' . $ilDB->quote($pass, 'integer') . ' ' .
			'AND question_fi = ' . $ilDB->quote($question->id, 'integer') . ' ' .
			'ORDER BY version_id DESC LIMIT ' . $ilDB->quote($version, 'integer');

		$res = $ilDB->query($query);
		$row = null;
		for ($i = 0; $i < $version; $i++) {
			if (!($row = $ilDB->fetchAssoc($res))) {
				return null;
			}
		}

		return $row;
	}

	/**
	 * @iliasmethod ilTestOutputGUI.urInTestAdministrationCmd
	 */

	public function urInTestAdministrationCmd($gui) {
		$_SESSION["urInTestAdministration"] = "requested";
		$gui->ctrl->redirect($gui, ilTestPlayerCommands::SHOW_QUESTION);
	}

	/**
	 * @iliasmethod ilTestOutputGUI.urEnterInTestAdministrationCmd
	 */

	public function urEnterInTestAdministrationCmd($gui) {
		require_once 'Customizing/uni-regensburg/classes/class.urAccount.php';
		$inspector = new urAccount($_POST["ur_admin_username"]);
		if ($inspector->authenticate($_POST["ur_admin_password"]) && $inspector->isAdministrator()) {
			$_SESSION["urInTestAdministration"] = "enabled";
			$_SESSION["urPreviousAnswerVersion"] = 1;
		}
		$gui->ctrl->redirect($gui, ilTestPlayerCommands::SHOW_QUESTION);
	}

	/**
	 * @iliasmethod ilTestOutputGUI.urCancelVersionCmd
	 */

	public function urCancelVersionCmd($gui) {
		unset($_SESSION["urInTestAdministration"]);
		$gui->ctrl->redirect($gui, ilTestPlayerCommands::SHOW_QUESTION);
	}

	/**
	 * @iliasmethod ilTestOutputGUI.urAcceptVersionCmd
	 */

	public function urAcceptVersionCmd($gui) {
		unset($_SESSION["urInTestAdministration"]);
		$gui->saveQuestionSolution();
		$gui->ctrl->redirect($gui, ilTestPlayerCommands::SHOW_QUESTION);
	}

	/**
	 * @iliasmethod ilTestOutputGUI.urGotoPreviousAnswerVersionCmd
	 */

	public function urGotoPreviousAnswerVersionCmd($gui) {
		$_SESSION["urPreviousAnswerVersion"] = strval(max(1, intval($_SESSION["urPreviousAnswerVersion"]) - 1));
		$gui->ctrl->redirect($gui, ilTestPlayerCommands::SHOW_QUESTION);
	}

	/**
	 * @iliasmethod ilTestOutputGUI.urGotoNextAnswerVersionCmd
	 */

	public function urGotoNextAnswerVersionCmd($gui) {
		$_SESSION["urPreviousAnswerVersion"] = strval(intval($_SESSION["urPreviousAnswerVersion"] + 1));
		$gui->ctrl->redirect($gui, ilTestPlayerCommands::SHOW_QUESTION);
	}
}
