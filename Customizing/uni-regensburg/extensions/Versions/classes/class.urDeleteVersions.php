<?php

require_once __DIR__ . '/../../../settings/interface.urSettingStorage.php';

class urDeleteVersions implements urSettingStorage {

	public function __construct($id, $defaultValue) {
	}

	public function get() {
		return false;
	}

	public function set($value) {
		if ($value) {
			require_once 'class.urVersions.php';
			urVersions::erase();
		}
	}
}
