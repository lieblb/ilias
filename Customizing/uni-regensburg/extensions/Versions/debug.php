<?php

// direct export via GET parameters for debugging saved answer states. do NOT enable on production!
// e.g. http://yourilias/ILIAS/Customizing/uni-regensburg/extensions/Versions/debug.php?client_id=ilias&question=Planeten

function readConfigForExtension($name) {
	$configYAML = file_get_contents(__DIR__ . '/../../config.yml');
	$parsedYAML = Symfony\Component\Yaml\Yaml::parse($configYAML);
	foreach ($parsedYAML['extensions'] as $ext) {
		if ($ext['name'] == $name) {
			return $ext;
		}
	}

	return array();
}

chdir("../../../..");

require_once 'libs/composer/vendor/autoload.php';

$config = readConfigForExtension('Versions');

if (!isset($config['unsafe_debug_export']) || $config['unsafe_debug_export'] !== true) {
	echo "ERROR";
	die;
}

try {
    require_once 'Services/Context/classes/class.ilContext.php';
    ilContext::init(ilContext::CONTEXT_WEB);

    require_once("./Services/Init/classes/class.ilInitialisation.php");
    ilInitialisation::initILIAS();

    function annotate(&$cell) {
		global $ilDB;
		if (isset($cell['question_fi']) && isset($cell['value1'])) {
			$questionId = intval($cell['question_fi']);

			$value1 = intval($cell['value1']);
			$value2 = intval($cell['value2']);

			// annotate for multiple choice questions.
			$set = $ilDB->queryF('select answertext from qpl_a_mc where question_fi=%s and aorder=%s',
				array('integer', 'integer'), array($questionId, $value1));
			while ($rec = $ilDB->fetchAssoc($set)) {
				$cell['answertext'] = $rec['answertext'];
				break;
			}

			// annotate for single choice questions.
			$set = $ilDB->queryF('select answertext from qpl_a_sc where question_fi=%s and aorder=%s',
				array('integer', 'integer'), array($questionId, $value1));
			while ($rec = $ilDB->fetchAssoc($set)) {
				$cell['answertext'] = $rec['answertext'];
				break;
			}

			// annotate for matching questions.
			$set = $ilDB->queryF('select * from qpl_a_matching where question_fi=%s',
				array('integer'), array($questionId));
			if ($rec = $ilDB->fetchAssoc($set)) {
				$termSet = $ilDB->queryF(
					'select term from qpl_a_mterm where question_fi=%s and ident=%s',
					array('integer', 'integer'), array($questionId, $value1));
				$termRecord = $ilDB->fetchAssoc($termSet);

				$definitionSet = $ilDB->queryF(
					'select definition from qpl_a_mdef where question_fi=%s and ident=%s',
					array('integer', 'integer'), array($questionId, $value2));
				$definitionRecord = $ilDB->fetchAssoc($definitionSet);

				$cell['answertext'] = sprintf('[%s] [%s]',
					$termRecord['term'], $definitionRecord['definition']);
			}
		}
    }

	function cellsHtml($cells) {
		$html[] = '<table>';
		foreach ($cells as $k => $v) {
			$html[] = '<tr>';
			$html[] = '<td>';
			$html[] = htmlspecialchars(json_encode($k));
			$html[] = '</td>';
			$html[] = '<td>';
			if (is_array($v)) {
				$html[] = cellsHtml($v);
			} else {
				$html[] = htmlspecialchars(json_encode($v));
			}
			$html[] = '</td>';
			$html[] = '</tr>';
		}
		$html[] = '</table>';
		return implode('', $html);
	}

	global $ilUser;
    $login = $ilUser->login;

    $questionTitle = $_GET["question"];

    // we assume that the user is only active in the test we're interested in.

    $sql = <<<EOT
    select version_id, tstamp, cells from ur_answer_versions where active_fi in (select active_id from tst_active where user_fi=(
      select usr_id from usr_data where login=%s)) and question_fi in (
        select question_id from qpl_questions where title=%s) order by version_id, tstamp;
EOT;

	global $ilDB;
    $set = $ilDB->queryF($sql, array('text', 'text'), array($login, $questionTitle));

    $html = array();
	$html[] = '<html>';
	$html[] = '<body>';

	$html[] = <<<EOT
	<style>
		table, th, td {
			border: 1px solid black;
			width: 100%;
		}
		th, td {
			padding: 1em;
		}
	</style>
EOT;

	$html[] = '<table>';
	$entryCount = 0;

	while ($rec = $ilDB->fetchAssoc($set)) {
		$cells = json_decode($rec["cells"], true);

		// annotate cell information for easier debugging.
		foreach ($cells as &$cell) {
			annotate($cell);
		}

		$html[] = '<tr>';

		$html[] = '<td>';
		$html[] = $rec['version_id'];
		$html[] = '</td>';

		$html[] = '<td>';
		$html[] = date('Y-m-d H:i:s', intval($rec['tstamp']));
		$html[] = '</td>';

		$html[] = '<td>';
		$html[] = cellsHtml($cells);
		$html[] = '</td>';

		$html[] = '</tr>';

		$entryCount += 1;
	}

	$html[] = '</table>';
	$html[] = '</body>';
	$html[] = '</html>';

	if ($entryCount > 0) {
		echo implode('', $html);
	} else {
		echo "no entries found.";
	}

} catch(\Exception $e) {
	echo $e;
    echo "ERROR";
	die;
}
