<?php

class urSkinExtension extends urExtension {

	private $hooks;

	public function __construct($hooks, $config)
	{
		$this->hooks = $hooks;
	}

	private function getSkinBasePath() {
		return "Customizing/uni-regensburg/extensions/Skin/skin/";
	}

	/**
	 * @iliashook ilUtil.getImagePath
	 * @param $img Name des Bildes, für das der Pfad geliefert werden soll
	 * @return bool|string Pfad zum Bild oder false, falls kein Override stattfinden soll
	 */

	public function overrideImagePath($img) {
		$path = $this->getSkinBasePath() . "images/" . $img;
		if (@file_exists($path)) {
			return $path;
		} else {
			return false;
		}
	}

	/**
	 * @iliashook ilTemplate.getTemplatePath
	 * @param $a_tplname
	 * @param $module_path
	 * @return string Pfad zum Template, das zu nutzen ist
	 */

	public function getCustomTemplatePath($a_tplname, $module_path) {
		return $this->getSkinBasePath() . $module_path . basename($a_tplname);
	}

	/**
	 * @iliashook ilTestPDFGenerator.getTemplatePath
	 * @param $a_tplname
	 * @param $module_path
	 * @return string Pfad zum Template, das zu nutzen ist
	 */

	public function getCustomTemplatePathForPDF($a_tplname, $module_path) {
		return $this->getCustomTemplatePath($a_tplname, $module_path);
	}
}
