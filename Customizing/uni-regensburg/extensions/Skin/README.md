Klinkt sich an verschiedene zentrale Stellen ein, um die UR-Custom-Skin (Customizing/uni-regensburg/skin) zu
implementieren. Im Gegensatz zum normalen ILIAS-Customizing-Vorgehen, erlauben diese Patches ein differentielles
Customizing, wo nur die Dateien abgelegt werden, die geändert werden sollen. 

E-Prüfungsrelevant am Customizing sind test_pdf.css und die monochrom gehaltenen Images für checkbox und radiobutton.
Insbesondere das Layout des Standard-PDF-Exports ist unsauber.