<?php

class urAutoPrintExtension extends urExtension {

	private $hooks;

	public function __construct($hooks, $config) {
		$this->hooks = $hooks;
	}

	public function createSettingGUI() {
		require_once __DIR__ . '/../../../settings/class.urCheckboxSettingGUI.php';

		return array(
			"Raumsteuerung BIB3" => array(
				new urCheckboxSettingGUI(
					"ur_enable_autoprint",
					array(
						"Automatisches Drucken",
						"Aktiviert im BIB3 das automatische Ausdrucken von Klausuren nach Abgabe.")
				)));
	}

	/**
	 *
	 * Toolbar, die bei Testeinsicht via Test > Teilnehmer > Detaillierte Ergebnisse zu erreichen ist. Wir blenden
	 * hier zusätzliche Buttons ein.
	 *
	 *
	 * @iliashook ilTestResultsToolbarGUI.build
	 * @param ilToolbarGUI $toolbar
	 * @return bool
	 */

	public function testResultsToolbarHook($toolbar)
	{
		if ($this->isAutoPrintEnabledForAdministrator() && strstr($toolbar->getPdfExportLinkTarget(), "outParticipantsPassDetails") != false) {
			require_once 'Services/UIComponent/Button/classes/class.ilLinkButton.php';

			$link = ilLinkButton::getInstance(); // always returns a new instance
			$link->setUrl($toolbar->getPdfExportLinkTarget() . "&" . "urAutoprintOnDemand=1");
			$link->setCaption("An BIB3-Drucker schicken", false);
			$link->setOmitPreventDoubleSubmission(true);

			$toolbar->addButtonInstance($link);
		} else if ($this->isAutoPrintEnabledForParticipant()) {
			$toolbar->addText("Ihre Antworten werden automatisch ausgedruckt.");
		}
	}

	/**
	 * Prüft, ob ein Automatisches Drucken ausgelöst werden soll und löst ihn ggf. aus.
	 *
	 * @iliashook ilTestEvaluationGUI.outParticipantsPassDetails
	 * @param ilCtrl $ctrl
	 * @param ilObjTest $testObject Test, der gedruckt werden soll
	 * @param integer $activeId Prüfungsinstanz im Test, die gedruckt werden soll
	 * @param integer $pass Prüfungsdurchlauf, der gedruckt werden soll
	 */

	public function autoPrintOnDemand($ctrl, $testObject, $activeId, $pass)
	{
		if (!isset($_GET["urAutoprintOnDemand"])) {
			return false;
		}
		if ($this->autoPrintHook($testObject, $activeId, $pass)) {
			global $ilDB, $lng;
			$participantData = new ilTestParticipantData($ilDB, $lng);
			$participantData->load($testObject->getTestId());
			$fullname = $participantData->getConcatedFullnameByActiveId($activeId);
			ilUtil::sendSuccess("Automatischer Ausdruck für $fullname wurde angestossen.", true);
			$ctrl->redirectByClass('ilObjTestGUI', 'infoScreen');
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Löst ein Automatisches Drucken aus.
	 *
	 * @iliashook ilTestPlayerAbstractGUI.performTestPassFinishedTasks
	 * @param ilObjTest $testObject
	 * @param integer $activeId
	 * @param integer $pass
	 * @return bool
	 */

	public function autoPrint($testObject, $activeId, $pass)
	{
		if (!$this->isAutoPrintEnabledForParticipant() && !$this->isAutoPrintEnabledForAdministrator()) {
			return false;
		}

		// ugly hack. sometimes $_GET["pdf"] is set and this makes ILIAS's PDF exporter create a
		// download even though we pass file output. so, clear this value here during our export.
		$oldPDF = isset($_GET["pdf"]) ? $_GET["pdf"] : "undefined";
		$_GET["pdf"] = 0;

		try {
			require_once __DIR__ . '/../../../classes/class.urTestPDF.php';
			$pdf = new urTestPDF($testObject);
			$pathToPDF = $pdf->exportOne($activeId, $pass);
			if ($pathToPDF === false) {
				return false;
			}

			@chmod($pathToPDF, 0775);
			/*shell_exec("lpr " . $pathToPDF);*/

			//unlink($pathToPDF);
		} finally {
			if ($oldPDF != "undefined") {
				$_GET["pdf"] = $oldPDF;
			}
		}

		return true;
	}

	private function isAutoPrintEnabledForParticipant()
	{
		global $ilSetting;

		if ($ilSetting->get("ur_enable_autoprint") == 0) {
			return false;
		}

		// Automatisches Drucken wird nur durchgeführt, falls wir im Prüfungsmodus sind und
		// der Teilnehmer tatsächlich in einem Prüfungsraum ist (andere am Campus durchgeführte
		// Tests werden also ignoriert).

		$examPlugin = $this->hooks->getExtension('Exam');

		if ($examPlugin && $examPlugin->isPrivilegedAssessmentParticipant()) {
			return true;
		} else {
			return true;
		}
	}

	private function isAutoPrintEnabledForAdministrator()
	{
		global $ilSetting;

		if ($ilSetting->get("ur_enable_autoprint") == 0) {
			return false;
		}

		return $this->isAdministrator();
	}
}
