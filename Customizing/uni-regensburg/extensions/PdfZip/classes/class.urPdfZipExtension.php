<?php

class urPdfZipExtension extends urExtension {

	/**
	 * @iliashook ilTestExportGUI.listExportFiles
	 * @param ilToolbarGUI $toolbar
	 */

	public function addExportButtons($toolbar)
	{
		$toolbar->addFormButton("Alle PDFs", "downloadPDFs");
	}

	/**
	 *
	 * @iliashook ilExportGUI.executeCommand
	 * @param string $cmd
	 * @param ilTestExportGUI $gui
	 */

	public function executeExportCommand($cmd, $gui)
	{
		switch ($cmd) {
			case "downloadPDFs": {
				$this->downloadPDFs($gui->obj);
				return true;
			} break;
		}

		return false;
	}

	/**
	 * @param $testObj
	 * @throws ilException
	 */

	private function downloadPDFs($testObj)
	{
		require_once __DIR__ . '/../../../classes/class.urTestPDF.php';
		$pdf = new urTestPDF($testObj);
		$path = $pdf->exportAll();
		if ($path !== false) {
			ilUtil::deliverFile($path, pathinfo($path, PATHINFO_BASENAME));
		}
	}
}
