Globale Tweaks am UI:

- Ausblenden von Permanentlinks für normale Nutzer, vor allem aus Sicherheitsgründen und zur Verringerung der
Angriffsfläche des Systems.

- Ausblenden des Sprachmenüs auf der Loginseite (führt bei Sprachtest zuweilen zu Verwirrung und mehrfachen
Logins führt).

- Anzeigen eines anderen Systemnamens beim Login.
