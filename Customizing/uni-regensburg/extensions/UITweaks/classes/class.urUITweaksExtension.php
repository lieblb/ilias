<?php

class urUITweaksExtension extends urExtension {
	private $hooks;

	public function __construct($hooks, $config) {
		$this->hooks = $hooks;
	}

	/**
	 *
	 * Prüft, ob auf der Login-Seite die Sprachauswahl Deutsch/Englisch aktiviert ist. Dies führt bei
	 * Sprachentests im BIB3 oft zu Verwirrung.
	 *
	 * @iliashook ilStartUpGUI.getLanguageSelection
	 * @return bool Sprachauswahl an?
	 */

	public function hideLanguageSelection()
	{
		$examPlugin = $this->hooks->getExtension('Exam');
		if ($examPlugin) {
			return $examPlugin->isPrivilegedAssessmentParticipant();
		} else {
			return false;
		}
	}

	/**
	 *
	 * Prüft ob der aktuelle Nutzer permanente Links sehen darf. Permanente Links sind ein
	 * Sicherheitsrisiko, da normale Nutzer so z.B. die ID des geschützten Testbereichs einfach
	 * ermitteln können.
	 *
	 * @iliashook ilPermanentLinkGUI.getHTML
	 * @return bool aktueller Nutzer darf permanente Links sehen?
	 */

	public function hidePermanentLinks()
	{
		return !$this->isAdministrator();
	}

	/**
	 * @iliashook ilStartUpGUI.initStandardLoginForm
	 */

	public function tweakLoginFormHook($form)
	{
		$form->setTitle("Willkommen im E-Prüfungssystem");
	}

	/**
	 * @iliashook ilObjTestGUI.getParticipantsSubTabs.addExtraTimeLink
	 * @return bool
	 */

	public function addExtraTimeLink($ilTabs, $gui, $ctrl)
	{
		$ilTabs->addSubTabTarget( "timing",
			$ctrl->getLinkTarget($gui,'timingOverviewNotAvailable'),
			array("timing", "timingOverviewNotAvailable"), "", ""
		);
	}

	/**
	 * @iliasmethod ilObjTestGUI.timingOverviewNotAvailableObject
	 */

	public function timingOverviewNotAvailableObject($gui)
	{
		ilUtil::sendInfo($gui->lng->txt("tst_extratime_notavailable"));
	}
}
