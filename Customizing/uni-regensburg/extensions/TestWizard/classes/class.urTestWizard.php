<?php

class urTestWizard {

	/**
	 * @var ilObjTest
	 */
	private $testObj;

	public function __construct($testObj) {
		$this->testObj = $testObj;
	}

	private function checkAutospeichernSettings() {
		$warn = array();

		if (!$this->testObj->getAutosave()) {
			return $warn;
		}

		global $ilDB;

		$set = $ilDB->queryF(<<<EOT
SELECT COUNT(*) FROM tst_test_question tq
	INNER JOIN qpl_questions q ON q.question_id=tq.question_fi
	WHERE test_fi=(SELECT test_id FROM tst_tests WHERE obj_fi=%s) AND question_type_fi IN (
		SELECT question_type_id FROM qpl_qst_type WHERE type_tag IN ("assClozeTest", "assTextQuestion"))
EOT
		, array("integer"), array($this->testObj->getId()));

		$count = intval(array_pop($ilDB->fetchAssoc($set)));
		if ($count > 0) {
			$warn[] = "Autospeichern ist nicht aktiviert, obwohl der Test Lücken- oder Freitexte " .
				"enthält. Sie müssen damit bei Rechnerabstürzen von vollständigen Datenverlusten ausgehen.";
		}

		return $warn;
	}

	private function checkTestdurchlaufSettings() {
		$warn = array();
		if ($this->testObj->getNrOfTries() != 1) {
			$warn[] = "Der Test ist nicht begrenzt auf einen Durchlauf";
		}
		if ($this->testObj->getShowCancel()) {
			$warn[] = "Die Testunterbrechung ist erlaubt";
		}
		if ($this->testObj->getEnableProcessingTime() != 1) {
			$warn[] = "Es ist keine Bearbeitungszeit gesetzt";
		}
		if ($this->testObj->getAnonymity()) {
			$warn[] = "Der Test ist auf anonyme Durchführung konfiguriert";
		}
		return $warn;
	}

	private function checkFeedbackSettings() {
		$warn = array();
		if ($this->testObj->getSpecificAnswerFeedback()) {
			$warn[] = "Spezifisches Antwortfeedback ist angeschaltet";
		}
		if ($this->testObj->getGenericAnswerFeedback()) {
			$warn[] = "Allgemeines Antwortfeedback ist angeschaltet";
		}
		if ($this->testObj->getAnswerFeedbackPoints()) {
			$warn[] = "Antwortfeedback-Punkte sind angeschaltet";
		}
		if ($this->testObj->getInstantFeedbackSolution()) {
			$warn[] = "Antwortfeedback-Lösung ist angeschaltet";
		}
		return $warn;
	}

	/**
	 * Prüft, ob die für E-Prüfungen empfohlenen Einstellungen gesetzt sind.
	 *
	 * @return array
	 */

	private function checkScoringSettings() {
		$warn = array();
		if ($this->testObj->getCountSystem() != COUNT_PARTIAL_SOLUTIONS) {
			$warn[] = "Teilweise richtige Lösungen werden nicht gezählt";
		}
		if ($this->testObj->getMCScoring() == SCORE_ZERO_POINTS_WHEN_UNANSWERED) {
			$warn[] = "Leere Antworten werden immer mit 0 bepunktet";
		}
		if ($this->testObj->getScoreCutting() != SCORE_CUT_QUESTION) {
			$warn[] = "Punkte werden unzulässig über Fragen verteilt";
		}
		if ($this->testObj->getPassScoring() != SCORE_LAST_PASS) {
			$warn[] = "Es wird nicht der letzte Durchgang bewertet";
		}
		return $warn;
	}

	private function checkErgebniszugangSettings() {
		$warn = array();
		if ($this->testObj->isScoreReportingEnabled()) {
			$warn[] = "Der Ergebniszugang für Teilnehmer ist angeschaltet";
		}
		if ($this->testObj->getMailNotification()) {
			$warn[] = "Es findet ein E-Mail-Versand statt";
		}
		return $warn;
	}

	private function getIndicator($ok) {
		if ($ok) {
			return '<span style="color:green;">&#x2714;</span>';
		} else {
			return '<span style="color:red;">&#x2716;</span>';
		}
	}

	public function buildWarningsForm($form) {
		$header = new ilFormSectionHeaderGUI();
		$header->setTitle("Schnellübersicht E-Prüfungen");
		$form->addItem($header);

		$criteria = array("Autospeichern", "Testdurchlauf", "Scoring", "Ergebniszugang", "Feedback");
		foreach ($criteria as $criterion) {
			$method = "check" . $criterion . "Settings";
			$warnings = $this->$method();
			$item = new ilNonEditableValueGUI($criterion, "", true);
			$item->setValue($this->getIndicator(empty($warnings)));
			if (!empty($warnings)) {
				if (count($warnings) > 1) {
					$ul = array();
					foreach ($warnings as $warning) {
						$ul[] = "- " . $warning;
					}
					$item->setInfo(implode("<br>", $ul));
				} else {
					$item->setInfo($warnings[0]);
				}
			}
			$form->addItem($item);
		}
	}

}