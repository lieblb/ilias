<?php

class urTestWizardExtension extends urExtension {

	/**
	 * @iliashook ilObjTestSettingsGeneralGUI.buildForm
	 * @param $form
	 * @param ilObjTest $testObj
	 */

	public function testSettingsFormHook($form, $testObj)
	{
		if (!$this->isAdministrator()) {
			return; // Für Nicht-Adminstratoren werden in keinem Fall UR-interne Warnhinweise angezeigt.
		}

		require_once 'class.urTestWizard.php';
		$wizard = new urTestWizard($testObj);
		$wizard->buildWarningsForm($form);
	}
}
