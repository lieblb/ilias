Zusätzliche Warnhinweise für die Konfiguration des Testes, um versehentliches
Fehlkonfigurieren des Tests zu verhindern.
 
Z.B. soll verhindert werden, dass bei Testdurchführungen mit Freitexten das Anschalten
des wichtigen Autospeicherns übersehen wird.
