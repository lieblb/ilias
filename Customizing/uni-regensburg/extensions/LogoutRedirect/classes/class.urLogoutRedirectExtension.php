<?php

class urLogoutRedirectExtension extends urExtension {

	private $hooks;

	public function __construct($hooks, $config)
	{
		$this->hooks = $hooks;
	}

	/**
	 *
	 * Liefert den Redirect, der beim Logout automatisch stattfinden soll, oder false, falls kein solcher
	 * Redirect stattfinden soll.
	 *
	 * @iliashook logout
	 * @return bool|string Auszugebender Redirect oder false
	 */

	public function logout() {
		$examPlugin = $this->hooks->getExtension('Exam');

		if ($examPlugin && $examPlugin->isPrivilegedAssessmentParticipant()) {
			return "<meta http-equiv=\"refresh\" content=\"0; URL=" .
				ILIAS_HTTP_PATH . sprintf("/login.php?client_id=%s&lang=de\">", CLIENT_ID);
		} else {
			return false;
		}
	}
}
