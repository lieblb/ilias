Automatischer Redirect zum Login nach Logout in Prüfungssituation. Hilfreich für einen
reibungslosen Prüfungsbetrieb (nach dem Logout eines Teilnehmers kann sofort der Login
des nächsten Teilnehmers erfolgen).
