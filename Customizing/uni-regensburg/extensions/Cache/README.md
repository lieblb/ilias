Experimental: a simple, file-based cache for reducing DB load for static data.

Language and ctrl data are currently read from the DB for each access. The
SQL commands for these show up in performance tests. The data read is quite
big (when serialized, ctrl data is about 512K + language.de.assessment
about 140K + various others).

In various scenarios, where the DB is under heavy load, accessing such data from
the file system (or from redis later on) might be a good idea.

-rw-rw-r-- 1 www-data www-data  24416 Oct 26 08:56 language.de.administration
-rw-rw-r-- 1 www-data www-data 135633 Oct 26 08:56 language.de.assessment
-rw-rw-r-- 1 www-data www-data   4145 Oct 26 08:56 language.de.obj
-rw-rw-r-- 1 www-data www-data   3112 Oct 26 08:56 language.de.form
-rw-rw-r-- 1 www-data www-data  10223 Oct 26 08:56 language.de.search
-rw-rw-r-- 1 www-data www-data   3863 Oct 26 08:56 language.de.buddysystem
-rw-rw-r-- 1 www-data www-data   3484 Oct 26 08:56 language.de.wsp
-rw-rw-r-- 1 www-data www-data    422 Oct 26 08:56 language.de.background_tasks
-rw-rw-r-- 1 www-data www-data   1678 Oct 26 08:56 language.de.awrn
-rw-rw-r-- 1 www-data www-data   1418 Oct 26 08:56 language.de.notes
-rw-rw-r-- 1 www-data www-data  15363 Oct 26 08:56 language.de.grp
-rw-rw-r-- 1 www-data www-data  13566 Oct 26 08:56 language.de.chatroom
-rw-rw-r-- 1 www-data www-data   2072 Oct 26 08:56 language.de.cont
-rw-rw-r-- 1 www-data www-data   3329 Oct 26 08:56 language.de.cntr
-rw-rw-r-- 1 www-data www-data   1302 Oct 26 08:56 language.de.tagging
-rw-rw-r-- 1 www-data www-data   1541 Oct 26 08:56 language.de.rating
-rw-rw-r-- 1 www-data www-data   3438 Oct 26 08:56 language.de.rep
-rw-rw-r-- 1 www-data www-data  25457 Oct 26 08:56 language.de.meta
-rw-rw-r-- 1 www-data www-data   7266 Oct 26 08:56 language.de.adve
-rw-rw-r-- 1 www-data www-data  10051 Oct 26 08:57 language.de.user
-rw-rw-r-- 1 www-data www-data    402 Oct 26 08:57 language.de.tbl
-rw-rw-r-- 1 www-data www-data  18043 Oct 26 08:57 language.de.mail
-rw-rw-r-- 1 www-data www-data    415 Oct 26 08:57 language.de.mmbr
-rw-rw-r-- 1 www-data www-data  45323 Oct 26 08:57 language.de.crs
-rw-rw-r-- 1 www-data www-data    315 Oct 26 08:57 language.de.contact
-rw-rw-r-- 1 www-data www-data    505 Oct 26 08:57 language.de.adm
-rw-rw-r-- 1 www-data www-data   7402 Oct 26 08:57 language.de.auth
-rw-rw-r-- 1 www-data www-data    974 Oct 26 08:57 language.de.pdesk
-rw-rw-r-- 1 www-data www-data   5248 Oct 26 08:57 language.de.pd
-rw-rw-r-- 1 www-data www-data  24106 Oct 26 08:57 language.de.dateplaner
-rw-rw-r-- 1 www-data www-data   6895 Oct 26 08:57 language.de.prtf
-rw-rw-r-- 1 www-data www-data   1256 Oct 26 08:57 language.de.tax
-rw-rw-r-- 1 www-data www-data   2019 Oct 26 08:57 language.de.lti
-rw-rw-r-- 1 www-data www-data      0 Oct 26 08:57 language.de.tst
-rw-rw-r-- 1 www-data www-data    580 Oct 26 08:57 language.de.copg
-rw-rw-r-- 1 www-data www-data  76161 Oct 26 08:57 language.de.content
-rw-rw-r-- 1 www-data www-data    175 Oct 26 08:57 language.de.link
-rw-rw-r-- 1 www-data www-data   6371 Oct 26 08:57 language.en.auth
-rw-rw-r-- 1 www-data www-data   2762 Oct 26 08:57 language.en.form
-rw-rw-r-- 1 www-data www-data   2280 Oct 26 08:57 language.de.chatroom_adm
-rw-rw-r-- 1 www-data www-data   7895 Oct 26 09:00 language.de.certificate
-rw-rw-r-- 1 www-data www-data    395 Oct 26 09:06 language.de.exp
drwxrwxr-x 2 www-data www-data   4096 Oct 26 11:22 .
-rw-r--r-- 1 www-data www-data 495820 Oct 26 11:22 ctrl