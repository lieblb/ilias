<?php

class urCacheExtension extends urExtension
{
	/**
	 * @var urHooks
	 */

	private $hooks;

	private $domains;

	private $basePath;

	public function __construct($hooks, $config) {
		$this->hooks = $hooks;
		$this->domains = array();
		if (isset($config['domains'])) {
			foreach ($config['domains'] as $d) {
				$this->domains[$d] = true;
			}
		}
		$this->basePath = rtrim(realpath($config['storage']), '/') . '/';
		if (!is_dir($this->basePath)) {
			if (!mkdir($this->basePath, 0777, true)) {
				error_log("could not create cache dir at " + $this->basePath);
			}
		}
	}

	private function getCacheFilePath($domain, $key) {
		$fullKey = array_merge(array($domain), $key);
		foreach ($fullKey as $part) {
			if (preg_match('/[a-zA-Z0-9]+/', $part) !== 1) {
				throw new Exception("illegal cache key " . var_export($fullKey, true));
			}
		}
		return $this->basePath . implode('.', $fullKey);
	}

	/**
	 * @iliashook cache.get
	 * @param $key
	 * @return bool|string
	 * @throws Exception
	 */

	public function get($domain, $key) {
		if ($this->domains[$domain]) {
			$path = $this->getCacheFilePath($domain, $key);
			if (file_exists($path)) {
				return @file_get_contents($path);
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	/**
	 * @iliashook cache.put
	 * @param $key
	 * @param $value
	 * @throws Exception
	 */

	public function put($domain, $key, $value) {
		if ($this->domains[$domain]) {
			$path = $this->getCacheFilePath($domain, $key);
			return @file_put_contents($path, $value);
		} else {
			return false;
		}
	}

	/**
	 * @iliashook cache.clear
	 */

	public function clear($domain, $key) {
		if ($this->domains[$domain]) {
			$path = $this->getCacheFilePath($domain, $key);
			if (file_exists($path)) {
				unlink($path);
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
}
