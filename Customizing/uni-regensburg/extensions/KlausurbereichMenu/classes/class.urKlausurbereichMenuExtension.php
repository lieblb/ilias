<?php

class urKlausurbereichMenuExtension extends urExtension {

	private $hooks;

	public function __construct($hooks, $config) {
		$this->hooks = $hooks;
	}

	/**
	 * @iliashook ilMainMenuGUI.renderMainMenuListEntries
	 * @param ilMainMenuGUI $mainMenu
	 */

	public function renderMainMenuHook($gui, $a_tpl) {
		$examPlugin = $this->hooks->getExtension('Exam');
		if ($examPlugin) {
			if ($examPlugin->isPrivilegedAssessmentParticipant() || $this->isAdministrator()) {
				$gui->renderEntry($a_tpl, "klausurbereich", "Klausurbereich",
					$gui->getScriptTarget($examPlugin->getKlausurbereichURL()), "_self");
			}
		}
	}
}