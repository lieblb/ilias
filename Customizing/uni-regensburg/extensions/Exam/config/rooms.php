<?php

// Schlüssel: Raumname
// Wert: IP-Bereich, durch den die Kategorie zugreifbar ist.

return array(
	'bib3' => array(132, 199, 240, array(0, 255)),
	'pt3' => array(132, 199, 34, array(60, 84))
);
