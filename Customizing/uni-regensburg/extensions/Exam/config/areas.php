<?php

// Definiert, aus welchen Räumen welcher Klausurbereich zugegriffen werden kann
// (falls der entsprechende Raum im Prüfungsmodus ist).

// Schlüssel: ILIAS-Kategorie-ID eines geschützten Testbereichs für Prüfungen
// (dies ist abhängig von der konkreten ILIAS-Installation).

// Wert: Array von Räumen (Raumnamen), aus denen der Bereich zugegriffen werden
// darf (wenn der Raum im Prüfungsmodus ist).

return array(
	1 => array('bib3'),
	1471 => array('pt3')
);
