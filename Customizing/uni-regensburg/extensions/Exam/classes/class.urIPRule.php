<?php

/**
 * Class urIPRule
 *
 * Implementiert einen IP-basierten booleschen Filter, der z.B. für die Prüfung genutzt wird,
 * ob ein Nutzer auf einem Prüfungsraum kommt oder nicht.
 */

class urIPRule {
	private $rule;

	public function __construct($r) {
		$this->rule = $r;
	}

	private function check($c, $r) {
		if (gettype($r) == 'array') {
			return intval($c) >= intval($r[0]) &&  intval($c) <= intval($r[1]);
		} else {
			return intval($c) == intval($r);
		}
	}

	public function matches() {
		$ip = $_SERVER['REMOTE_ADDR'];

		if (preg_match('/[0-9]+\\.[0-9]+\\.[0-9]+\\.[0-9]+/', $ip) !== 1) {
			return false;
		}

		$components = explode(".", $ip);
		if (count($components) != 4) {
			return false;
		}

		for ($i = 0; $i < 4; $i++) {
			if (!$this->check($components[$i], $this->rule[$i])) {
				return false;
			}
		}

		return true;
	}
}
