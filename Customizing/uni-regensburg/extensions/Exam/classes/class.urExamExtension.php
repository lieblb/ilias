<?php

class urExamExtension extends urExtension {

	private $rooms;
	private $areas;

	public function __construct($hooks, $config) {
		require_once "class.urIPRule.php";
		$rooms = array();
		foreach ($config['rooms'] as $room => $r) {
			$rooms[$room] = new urIPRule($r);
		}
		$this->rooms = $rooms;
		$this->areas = $config['areas'];
	}

	public function getTestAreaIds() {
		return array_keys($this->areas);
	}

	public function createSettingGUI() {
		require_once __DIR__ . '/../../../settings/class.urCheckboxSettingGUI.php';

		$ui = array(
			"Testschutz" => array(
				new urCheckboxSettingGUI(
					"ur_disable_room_isolation",
					array(
						"Raumisolation aufheben",
						"Wendet das für Prüfungsräume reservierte Systemverhalten überall an und macht den gesamten " .
							"Campus zum freigeschalteten Prüfungsraum. ACHTUNG: nur im Notfall aktivieren, da hiermit " .
							"von überall auf die speziell geschützten Klausurbereiche zugegriffen werden kann.")
				)
			)
		);

		foreach (array_keys($this->rooms) as $room) {
			$ui["Raumsteuerung " . strtoupper($room)] = array(
				new urCheckboxSettingGUI(
					"ur_assessment_mode_" . $room,
					array(
						"E-Prüfungsmodus für " . strtoupper($room),
						"Erlaubt den Zugriff auf E-Prüfungsdaten aus dem Raum " . strtoupper($room))
				));
		}

		return $ui;
	}

	/**
	 * Ermittelt, ob der Prüfungsmodus samt Firewall etc. aktiv ist, d.h. ob der momentane
	 * Benutzer ein Teilnehmer einer E-Prüfung in einem E-Prüfungsraum ist.
	*
	 * Prüft dazu, ob der aktuelle Benutzer aus dem Prüfungsraum kommt. Konkret wird dies
	 * über eine  Prüfung der IP ermittelt.
	 *
	 * @return bool User aus Prüfungsraum, der momentan im Prüfungsmodus ist?
	 */

	public function isPrivilegedAssessmentParticipant()
	{
		global $ilSetting;

		if ($ilSetting->get("ur_disable_room_isolation") == 1) {
			return true;
		}

		foreach ($this->rooms as $room => $rule) {
			if ($this->isFromAssessmentRoom($room)) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Prüft, ob ein gegebener Test im speziellen für die Testdurchführung gedachten Testbereich
	 * (oder einem darunter liegenden Ordner, wie einem Weiterleitungsordner) liegt.
	 *
	 * @param ilObjTest $test zu prüfender Test
	 * @return bool liegt der Test in einem geschützten Testbereich?
	 */

	public function mayAccessTestAsParticipant($test) {
		global $tree;
		$refId = $test->getRefId();

		$visited = array();

		while ($refId !== ROOT_FOLDER_ID) {
			$refId = intval($tree->getParentId($refId));

			if (isset($this->areas[$refId])) {
				foreach ($this->areas[$refId] as $room) {
					if ($this->isFromAssessmentRoom($room)) {
						return true;
					}
				}
			}

			if (isset($visited[$refId])) {
				break; // quit this loop, we're at the root.
			}
			$visited[$refId] = true;
		}

		return false;
	}

	/**
	 * @param $room
	 * @return bool Kommt der aktuelle Benutzer aus dem Raum $room?
	 */

	private function isFromAssessmentRoom($room) {
		if (isset($this->rooms[$room]) && $this->rooms[$room]->matches()) {
			global $ilSetting;
			if ($ilSetting->get("ur_assessment_mode_" . $room)) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Liefert die URL des für einen konkreten Benutzers passenden Klausurbereichs. Hängt von der IP des
	 * Benutzers ab (d.h. dem Raum, in dem er sich befindet).
	 *
	 * @return string URL
	 */

	public function getKlausurbereichURL()
	{
		foreach ($this->areas as $catId => $rooms) {
			foreach ($rooms as $room) {
				if ($this->isFromAssessmentRoom($room)) {
					return 'goto.php?target=cat_' . $catId;
				}
			}
		}

		return false;
	}
}
