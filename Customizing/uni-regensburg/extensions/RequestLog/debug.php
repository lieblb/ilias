<?php

// direct export via GET parameters for debugging. do NOT enable on production!
// e.g. http://yourilias/ILIAS/Customizing/uni-regensburg/extensions/RequestLog/debug.php?client_id=ilias

function readConfigForExtension($name) {
	$configYAML = file_get_contents(__DIR__ . '/../../config.yml');
	$parsedYAML = Symfony\Component\Yaml\Yaml::parse($configYAML);
	foreach ($parsedYAML['extensions'] as $ext) {
		if ($ext['name'] == $name) {
			return $ext;
		}
	}

	return array();
}

chdir("../../../..");

require_once 'libs/composer/vendor/autoload.php';

$config = readConfigForExtension('Versions');

if (!isset($config['unsafe_debug_export']) || $config['unsafe_debug_export'] !== true) {
	echo "ERROR";
	die;
}

try {
	require_once 'Services/Context/classes/class.ilContext.php';
	ilContext::init(ilContext::CONTEXT_WEB);

	require_once("./Services/Init/classes/class.ilInitialisation.php");
	ilInitialisation::initILIAS();

	global $ilUser;
	$login = $ilUser->login;

	$sql = <<<EOT
	SELECT * FROM ur_request_debug_log WHERE user=%s ORDER BY entry_id
EOT;

	global $ilDB;
	$set = $ilDB->query(sprintf($sql, $ilDB->quote($login, "text")));

	$errors = array();

	function format_plain($data) {
		return htmlspecialchars($data);
	}

	function format_uri($data) {
		return implode('<br>&', array_map(htmlspecialchars, explode('&', $data)));
	}

	function format_generic($data) {
		if (is_string($data)) {
			return format_uri($data);
		} else {
			return format_json(json_encode($data));
		}
	}

	function format_timestamp($data) {
		$t = intval($data) / 1000;
		return date('Y/m/d', $t) . '<br>' . date('H:i:s', $t) . '<br>' . $t;
	}

	function format_json($data) {
		try {
			$html = array();
			$json = json_decode($data, true);
			if (!empty($json)) {
				if (!is_array($json)) {
					return '?' . htmlspecialchars($data);
				}
				$html[] = '<table>';
				foreach ($json as $k => $v) {
					$html[] = '<tr>';
					$html[] = '<td>' . htmlspecialchars(json_encode($k)) . '</td>';
					$html[] = '<td>' . format_generic($v) . '</td>';
					$html[] = '</tr>';
				}
				$html[] = '</table>';
			}
			return implode('', $html);
		} catch(\Exception $e) {
			return '?' . htmlspecialchars($data);
		}
	}

	$keys = array(
		'uri' => format_uri,
		'data' => format_json,
		'started' => format_timestamp,
		'finished' => format_timestamp,
		'n_commits' => format_plain,
		'last_error' => format_json);

	$html = array();
	$html[] = '<html>';
	$html[] = '<body>';

	$html[] = <<<EOT
	<style>
		table, th, td {
			border: 1px solid black;
			width: 100%;
		}
		th, td {
			padding: 1em;
		}
	</style>
EOT;

	$html[] = '<table>';

	while ($rec = $ilDB->fetchAssoc($set)) {
		$html[] = '<tr>';
		foreach ($keys as $k => $format) {
			$html[] = '<td>';
			$html[] = $format($rec[$k]);
			$html[] = '</td>';
		}
		$html[] = '</tr>';
	}

	$html[] = '</table>';
	$html[] = '</body>';
	$html[] = '</html>';
	echo implode('', $html);

} catch(\Exception $e) {
	echo $e;
	echo "ERROR";
	die;
}


