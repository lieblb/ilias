<?php

class urRequestLogExtension {
	private $hooks;
	private $config;
	private $enabled;

	public function __construct($hooks, $config) {
		$this->hooks = $hooks;
		$this->config = $config;
		$this->enabled = isset($config['enabled']) && $config['enabled'];
	}

	/**
	 * Install the DB table used for storing details about each request.
	 */

	public static function install() {
		global $ilDB;
		if (!$ilDB->tableExists('ur_request_debug_log')) {
			$fields = array(
				'entry_id' => array('type' => 'integer', 'length' => 8, 'notnull' => true),
				'user' => array('type' => 'text', 'length' => 256, 'notnull' => true),
				'uri' => array('type' => 'text', 'length' => 1024, 'notnull' => true),
				'data' => array('type' => 'blob', 'notnull' => true),
				'started' => array('type' => 'integer', 'length' => 8, 'notnull' => true),
				'finished' => array('type' => 'integer', 'length' => 8, 'notnull' => false),
				'n_commits' => array('type' => 'integer', 'length' => 4, 'notnull' => true),
				'last_error' => array('type' => 'blob', 'notnull' => false)
			);
			$ilDB->createTable('ur_request_debug_log', $fields);
			$ilDB->addPrimaryKey('ur_request_debug_log', array('entry_id'));
			$ilDB->createSequence('ur_request_debug_log');
			$ilDB->addIndex('ur_request_debug_log', array('user', 'started'), 'i1');
		}
	}

	/**
	 * @iliashook globalEntry
	 */

	public function beginRequest() {
		if (!$this->enabled) {
			return;
		}

		self::install();

		global $ilUser;
		$username = $ilUser ? $ilUser->getLogin() : 'undefined';
		$username = $username ? $username : 'undefined';

		$data = json_encode($_POST);

		global $ilDB;
		$entryId = $ilDB->nextId("ur_request_debug_log");

		$GLOBALS['urRequestLogId'] = $entryId;

		$ilDB->manipulateF(
			"INSERT INTO ur_request_debug_log(entry_id, user, uri, data, started, n_commits) VALUES (%s,%s,%s,%s,%s, %s)",
			array("integer", "text", "text", "blob", "integer", "integer"),
			array($entryId, $username, $_SERVER['REQUEST_URI'], $data, microtime(true) * 1000, 0));

		register_shutdown_function(function() {
			$this->hooks->executeHooks('globalExit', array(error_get_last()), 'first');
		});
	}

	/**
	 * @iliashook globalExit
	 */

	public function endRequest($error) {
		if (!$this->enabled) {
			return;
		}

		$entryId = $GLOBALS['urRequestLogId'];

		global $ilDB;
		$ilDB->manipulateF(
			"UPDATE ur_request_debug_log SET finished=%s, last_error=%s WHERE entry_id=%s",
			array("integer", "blob", "integer"),
			array(microtime(true) * 1000, json_encode($error), $entryId));
	}

	/**
	 * @iliashook databaseCommitEvent
	 */

	public function databaseCommitEvent() {
		if (!$this->enabled) {
			return;
		}

		$entryId = $GLOBALS['urRequestLogId'];

		// note: we assume the commit actually happens - otherwise our log won't commit either,
		// since we use the same DB connection :-(
		global $ilDB;
		$ilDB->manipulateF(
			"UPDATE ur_request_debug_log SET n_commits=n_commits + 1 WHERE entry_id=%s",
			array("integer"),
			array($entryId));
	}

	/**
	 * Delete all request log data of a user upon logout.
	 *
	 * @iliashook logout
	 */

	public function logout() {
		global $ilUser;
		$username = $ilUser ? $ilUser->getLogin() : null;

		if ($username !== null) {
			global $ilDB;

			$ilDB->manipulateF(
				"DELETE FROM ur_request_debug_log WHERE user=%s",
				array("text"), array($username));
		}

		return false;
	}
}
