Schreibt und liest Testergebnisse in transaktionssicherer Weise; die Standardimplementierung
des ILIAS-Testmoduls ist nicht sicher:

(1) Bei der Table-Lock-Methode ist u.a. nicht sichergestellt, dass bei Abbruch der Transaktion
durch Serverabsturz oder Timeouts der aktuelle Zustand zurückgerollt wird, was inkonsistente
Zustände hinterlassen kann.
(2) Die File-Lock-Methode ist zwar schnell, allerdings ist ein Sync zwischen Dateien und
Operationen auf der DB nicht gewährleistet, daher handelt es sich nicht um einen Lock ohne
race conditions.

Neue Klassen:

(1) eine angepasste InnoDB-Datenbankimplementierung (urDBPdoMySQLInnoDB).
(2) ein angepasster test process locker (urAssQuestionProcessLockerDb).
(3) ein von letzterem benutzter angepasster atom query (urNestableAtomQueryTransaction).

(3) kommt im Gegensatz zum normalen ilAtomQueryTransaction mit geschachtelten Transaktionen klar,
wie sie Testmodul verwendet werden - hierbei wird auf der obersten Ebene eine Transaktion begonnen,
innere Ebenen werden ignoriert; tritt also ein Fehler auf, wird die oberste Transaktion vollständig
zurückgerollt.

Anpassungen an ILIAS:

(A) Anpassung von ilDBWrapperFactory zur Erzeugung von urDBPdoMySQLInnoDB.

(B) Anpassung von ilAssQuestionProcessLockerFactory zur Erzeugung von urAssQuestionProcessLockerDb.

(C) Außerdem ist sind drei Calls zu von removeIntermediateSolution() anzupassen
(vgl. https://github.com/ILIAS-eLearning/ILIAS/pull/1223). Ohne diese Anpassungen kommt es zu
deadlocks und sehr vielen retries (niedrige Performance).
