<?php

require_once 'Services/PHPUnit/classes/class.ilUnitUtil.php';
require_once 'class.urTransactionOptions.php';

class urNestableAtomQueryTransaction extends ilAtomQueryTransaction {

	/**
	 * @var urTransactionOptions
	 */

	private $options = null;

	public function setTransactionOptions($options) {
		$this->options = $options;
	}

	/**
	 * @throws \ilAtomQueryException
	 */
	protected function runWithTransactions() {
		if ($this->ilDBInstance->inTransaction()) {
			$this->runQueries();
			return;
		}

		$startTime = microtime(true);
		$numRetries = 0;

		do {
			$this->ilDBInstance->beginTransaction();
			$e = null;
			try {
				try {
					$this->runQueries();
					$this->ilDBInstance->commit();
				} catch (PDOException $e) {
					throw new ilDatabaseException(
						"PDOException:\n" . $e->getMessage());
				}
			} catch (ilDatabaseException $e) {
				$this->ilDBInstance->rollback();

				if ($this->options == null) {
					$this->options = new urTransactionOptions();
				}

				$maxRetryDuration = $this->options->getMaxRetryDuration();
				$minRetryCount = $this->options->getMinRetryCount();

				// we try repeatedly for "maxRetryDuration" seconds until we give up.
				$dt = microtime(true) - $startTime;
				if ($dt > $maxRetryDuration && $numRetries >= $minRetryCount) {
					throw new ilDatabaseException(
						sprintf("Transaction failed after retrying %d times for %fs: ", $numRetries, $dt) .
						$e->getMessage());
				}

				if ($this->options->useExponentialBackoff()) {
					// exponential backoff (1ms, 2ms, 4ms, ...) if not too late.
					$dt = min(1000000 * ($maxRetryDuration - $dt), 1000 * pow(2, $numRetries));
					// usleep will abort PHP with an error if $dt == 0.
					if ($dt > 0) {
						usleep($dt);
					}
				}
			}

			$numRetries++;
		} while ($e instanceof ilDatabaseException);

		if ($numRetries >= $this->options->getGoodRetriesLogLevel()) {
			error_log(sprintf("Transaction succeeded after %d retries", $numRetries));
		}
	}
}
