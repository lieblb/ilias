<?php

require_once 'Modules/TestQuestionPool/classes/class.ilAssQuestionProcessLockerDb.php';

// a variant of ilAssQuestionProcessLockerDb that ALWAYS uses atom queries
// to execute its operations.

class urAssQuestionProcessLockerDb extends ilAssQuestionProcessLockerDb {

	protected function executeOperation(callable $operation) {
		// standard ilAssQuestionProcessLockerDb does not use an atom query
		// on saveworkingstate(). fix this here.

		if (!$this->atom_query) {
			$this->atom_query = $this->db->buildAtomQuery();

			// we need to add a (fake) table lock here to make ilAtomQuery's
			// base implementation happy. urNestableAtomQueryTransaction will
			// actually ignore this later on.
			$this->atom_query->addTableLock('tst_solutions');
		}

		parent::executeOperation($operation);
	}
}
