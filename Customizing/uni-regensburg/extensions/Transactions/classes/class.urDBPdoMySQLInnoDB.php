<?php

require_once 'Services/Database/classes/PDO/class.ilDBPdoMySQL.php';
require_once 'class.urTransactionOptions.php';

class urDBPdoMySQLInnoDB extends ilDBPdoMySQLInnoDB
{
	/**
	 * @var bool
	 *
	 * are we currently inside an open transaction?
	 */

	private $inTransaction;

	/**
	 * @var urTransactionOptions
	 */

	private $transactionOptions;

	/**
	 * urDBPdoMySQLInnoDB constructor.
	 * @param urTransactionOptions $transactionOptions
	 */

	public function __construct($transactionOptions)
	{
		$this->inTransaction = false;
		$this->transactionOptions = $transactionOptions;
	}

	public function inTransaction()
	{
		return $this->inTransaction;
	}

	public function beginTransaction()
	{
		parent::beginTransaction();
		$this->inTransaction = true;
	}

	public function commit()
	{
		global $urHooks;
		$urHooks->executeHooks('databaseCommitEvent', array());

		$this->inTransaction = false;
		parent::commit();
	}

	public function rollback()
	{
		global $urHooks;
		$urHooks->executeHooks('databaseRollbackEvent', array());

		$this->inTransaction = false;
		parent::rollback();
	}

	/**
	 * @param string $table_name
	 * @return int
	 */
	public function nextId($table_name)
	{
		// this is a modified version of the standard ILIAS MySQL sequence getter under
		// ilDBPdoMySQL.nextId().

		$sequence_name = $this->quoteIdentifier($this->getSequenceName($table_name), true);
		$seqcol_name = 'sequence';
		$query = "INSERT INTO $sequence_name ($seqcol_name) VALUES (NULL)";
		try {
			$this->pdo->exec($query);
		} catch (PDOException $e) {
			// no such table check
		}

		$result = $this->query('SELECT LAST_INSERT_ID() AS next');
		$value = $result->fetchObject()->next;

		if (is_numeric($value)) {
			// this version will modifies 'DELETE FROM' from the original ilDBPdoMySQL.nextId(),
			// as the latter causes a table lock that will causes our transactions to fail with
			// deadlocks very frequently.

			// InnoDB uses row locking for DELETE FROM; by keeping a safety margin of a number
			// of ids, we make sure we don't delete any ids that are the current $value in some
			// other actor. This seems to drastically improves the performance. We still do have
			// concurrency for deleting older ids though.

			$numberOfConcurrentReaders = intval($this->transactionOptions->getNumConcurrentReaders());
			$safeToDelete = intval($value - $numberOfConcurrentReaders);
			$query = "DELETE FROM $sequence_name WHERE $seqcol_name < $safeToDelete";
			$this->pdo->exec($query);
		}

		return $value;
	}

	/**
	 * @return bool
	 */
	public function supportsTransactions()
	{
		return true;
	}

	/**
	 * @return \ilAtomQuery
	 */
	public function buildAtomQuery()
	{
		require_once('class.urNestableAtomQueryTransaction.php');
		$query = new urNestableAtomQueryTransaction($this);
		$query->setTransactionOptions($this->transactionOptions);
		return $query;
	}
}
