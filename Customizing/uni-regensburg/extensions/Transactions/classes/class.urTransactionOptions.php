<?php

class urTransactionOptions {
	private $maxRetryDuration;
	private $minRetryCount;
	private $useExponentialBackoff;
	private $goodRetriesLogLevel;
	private $numConcurrentReaders;

	public function __construct($config) {
		if ($config && isset($config['maxRetryDurationInSeconds'])) {
			$this->maxRetryDuration = $config['maxRetryDurationInSeconds'];
		} else {
			$this->maxRetryDuration = 1; // 1s
		}

		if ($config && isset($config['minRetryCount'])) {
			$this->minRetryCount = $config['minRetryCount'];
		} else {
			$this->minRetryCount = 1;
		}

		if ($config && isset($config['useExponentialBackoff'])) {
			$this->useExponentialBackoff = $config['useExponentialBackoff'];
		} else {
			$this->useExponentialBackoff = true;
		}

		if ($config && isset($config['goodRetriesLogLevel'])) {
			$this->goodRetriesLogLevel = $config['goodRetriesLogLevel'];
		} else {
			$this->goodRetriesLogLevel = 1;
		}

		if ($config && isset($config['numConcurrentReaders'])) {
			$this->numConcurrentReaders = $config['numConcurrentReaders'];
		} else {
			$this->numConcurrentReaders = 100;
		}
	}

	public function getMaxRetryDuration() {
		return $this->maxRetryDuration;
	}

	public function getMinRetryCount() {
		return $this->minRetryCount;
	}

	public function useExponentialBackoff() {
		return $this->useExponentialBackoff;
	}

	public function getGoodRetriesLogLevel() {
		return $this->goodRetriesLogLevel;
	}

	public function getNumConcurrentReaders() {
		return $this->numConcurrentReaders;
	}
}
