<?php

require_once 'Modules/Test/classes/class.ilObjAssessmentFolder.php';
require_once 'class.urTransactionOptions.php';

class urTransactionsExtension extends urExtension {

	private $hooks;

	private $config;

	public function __construct($hooks, $config)
	{
		$this->hooks = $hooks;
		$this->config = $config;
	}

	public static function isEarlyBird()
	{
		return true;
	}

	/**
	 * @iliashook urWarningsExtension.getGlobalWarnings
	 * @param $form
	 * @param ilObjTest $testObj
	 */

	public function getGlobalWarnings()
	{
		$settings = new ilSetting('assessment');

		if ($settings->get('ass_process_lock_mode', ilObjAssessmentFolder::ASS_PROC_LOCK_MODE_NONE) != ilObjAssessmentFolder::ASS_PROC_LOCK_MODE_DB) {
			return array(array('error', 'Antworten werden nicht transaktionssicher gespeichert. Bitte schalten Sie Datenbanktabellen-Locks ein.'));
		}

		return array();
	}

	/**
	 * @iliashook ilDBWrapperFactory.getWrapper
	 * @param $a_type
	 * @return urDBPdoMySQLInnoDB
	 * @throws Exception
	 */

	public function createDatabaseWrapper($a_type)
	{
		switch ($a_type) {
			case ilDBConstants::TYPE_PDO_MYSQL_INNODB:
			case ilDBConstants::TYPE_INNODB:
				require_once('class.urDBPdoMySQLInnoDB.php');
				$options = new urTransactionOptions($this->config);
				return new urDBPdoMySQLInnoDB($options);

			default:
				throw new Exception("unsupported database type " . $a_type);
		}
	}

	/**
	 * @iliashook ilAssQuestionProcessLockerFactory.getLocker
	 * @param $db
	 * @param $lockMode
	 * @param $isAssessmentLogEnabled
	 * @return bool|urAssQuestionProcessLockerDb
	 */

	public function getQuestionProcessLocker($db, $lockMode, $isAssessmentLogEnabled) {

		if ($lockMode == ilObjAssessmentFolder::ASS_PROC_LOCK_MODE_DB) {
			require_once 'class.urAssQuestionProcessLockerDb.php';
			return new urAssQuestionProcessLockerDb($db);
		} else {
			return false;
		}
	}
}
