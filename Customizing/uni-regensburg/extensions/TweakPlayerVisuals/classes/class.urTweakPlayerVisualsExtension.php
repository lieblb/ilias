<?php

class urTweakPlayerVisualsExtension extends urExtension {

	public function isEnabled() {
		global $ilSetting;
		return $ilSetting->get("ur_tweak_player_visuals") != 0;
	}

	public function createSettingGUI() {
		require_once __DIR__ . '/../../../settings/class.urCheckboxSettingGUI.php';

		return array(
			"Antwortversionen" => array(
				new urCheckboxSettingGUI(
					"ur_tweak_player_visuals",
					array(
						"UR-spezifisches Testplayer-Aussehen",
						"Größere Schrift und ähnliche Tweaks.")
				)
			)
		);
	}

	/**
	 * @iliashook ilTestPlayerAbstractGUI.prepareTestPage
	 * @param ilTemplate $tpl
	 * @param bool $inKioskMode
	 */

	public function configureTestPlayerTemplate($tpl, $inKioskMode) {
		if ($inKioskMode) {
			$path = $this->getLocalJavaScriptPath(__DIR__ . '/../js/urTweakPlayerVisuals.js');
			$tpl->addJavascript($path, false);
		}
	}
}
