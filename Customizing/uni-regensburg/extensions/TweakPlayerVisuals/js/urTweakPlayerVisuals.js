// patching the css early on avoids annoying flickering.
for (var i = 0; i < document.styleSheets.length; i++) {
    for (var j = 0; j < document.styleSheets[i].cssRules.length; j++) {
        var rule = document.styleSheets[i].cssRules[j];
        if (rule.cssText.startsWith('.kiosk')) {
            rule.style.maxWidth = '800px';
            rule.style.margin = 'auto';
            rule.style.fontSize = '16px';
        }
    }
}
