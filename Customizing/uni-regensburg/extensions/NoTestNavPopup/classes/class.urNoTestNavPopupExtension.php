<?php

class urNoTestNavPopupExtension extends urExtension {
	/**
	 * @iliashook ilTestPlayerAbstractGUI.populateNavWhenChangedModal
	 * @return bool
	 */

	public function preventTestNavigationPopupHook() {
        return true;
	}
}
