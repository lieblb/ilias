Überblick über alle extensions/patches:

# Patch 0: urHooks

# Patch 1: Custom Skin

# Patch 2: Permamentlinks

# Patch 3: Redirect beim Logout

# Patch 4: Redirect nach Login

# Patch 5: Startup-GUI anpassen

# Patch 6: Zusätzliche Sicherungen beim Starten von Tests

# Patch 7: Link Klausurbereich in Main Menu

# Patch 8: Navigations-Popup verhindern

# Patch 9: Warnhinweis bei nicht aktiviertem Autospeichern

# Patch 10: Warnhinweis bei befüllten Klausurbereich

# Patch 11: Weiterleitung

# Patch 12: Zusätzliche Translations ohne Anpassung der zentralen Languagedateien

# Patch 13: Custom-Einstellungen unter Adminstration / Allgemein / E-Prüfungen

# Patch 14. Autodrucken (und alter UR-PDF-ZIP-Export)

# Patch 15. Verhindern des versehentlichen Löschens von Textblöcken

# Patch 16. Antwortversionierung samt speziellem Admin-Mode im Test.

# Patch 17. Anzeige von Logdateien im UI.

# Patch 18. Zusätzliche DB-Constraints und Test-Integritäts-Prüfungen.

# Patch 19. Zeitverlängerungs-Tab immer anzeigen.

vgl. https://mantis.ilias.de/view.php?id=23507

Das Zeitverlängerungs-Tab wird nur manchmal eingeblendet.

Diese Funktionalität wurde im Trunk neu implementiert.

# Patch 20. Schnellübersicht zu Testeinstellungen.

# Patch 21. Transaktionssichere Antwortspeicherung.


