<?php

class urTranslationsExtension extends urExtension {

	/**
	 * @var array Array von Lokalisierungstexten fürs Deutsche
	 */

	private $translations;

	public static function isEarlyBird()
	{
		return true;
	}

	public function __construct($hooks, $config) {
		$this->translations = array();
	}

	public function addTranslation($key, $text) {
		$this->translations[$key] = $text;
	}

	/**
	 * @iliashook ilLanguage.txt
	 * @param $id
	 */

	public function getTranslation($id) {
		if (isset($this->translations[$id])) {
			return $this->translations[$id];
		}
		return false;
	}
}
