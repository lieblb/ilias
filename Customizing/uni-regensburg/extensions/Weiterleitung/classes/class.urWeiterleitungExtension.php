<?php

class urWeiterleitungExtension extends urExtension {

	/**
	 *
	 * z.B. "UNIcert:cat_654"
	 *
	 * @iliashook ilTestGradingMessageBuilder.buildGradingMarkMsg
	 * @param $mark
	 * @return bool|string
	 *
	 */

	public function getWeiterleitungMessageHook($mark)
	{
		if (preg_match("/:/", $mark)) {
			$link = explode(":", $mark);
			if (count($link) == 2) {
				$html = "<a href=\"" . ILIAS_HTTP_PATH . "/goto.php?target=" . $link[1] . "\">";
				$html .= "weiter zum Fortsetzungstest \"" . $link[0] . "\"</a>";
				return "<p>Sie können nun " . $html . ".</p>";
			}
		}

		return false;
	}
}
