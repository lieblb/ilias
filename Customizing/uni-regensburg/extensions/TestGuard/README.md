Da auf dem System - auch in Folge des Einsystembetriebs - leichter produktive Tests aus Versehen an die Öffentlichkeit
gelangen könnten, wird das Starten von Tests zusätzlichen Rechteprüfungen unterzogen.
