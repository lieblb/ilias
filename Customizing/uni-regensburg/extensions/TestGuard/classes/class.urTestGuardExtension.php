<?php

class urTestGuardExtension extends urExtension {

	private $hooks;

	public function __construct($hooks, $config)
	{
		$this->hooks = $hooks;
	}

	public function createSettingGUI() {
		require_once __DIR__ . '/../../../settings/class.urCheckboxSettingGUI.php';

		return array(
			"Testschutz" => array(
				new urCheckboxSettingGUI(
					"ur_disable_test_protection",
					array(
						"Erweiterten Testzugangsschutz aufheben",
						"Deaktiviert Sicherheitsprüfungen für das Starten von Tests. Ermöglicht u.a. das Starten " .
						"von Tests, die in geschützten Bereichen liegen, auch außerhalb der definierten Prüfungsräume."
					)
				)
			)
		);
	}

	/**
	 *
	 * Implementiert ein restriktiveres Rechtesystem für Tests.
	 *
	 * Dient (a) zur Sicherung von Klausuren während der Durchführung im BIB3 (so dass kein
	 * Dritter während dieser Zeit Einsicht in die Klausur nehmen kann), und (b) zur Sicherheit
	 * dagegen, dass aus Versehen in einem Erstellungsbereich die Rechte für normale Benutzer
	 * nicht entzogen wurden (was versehentlich passieren könnte).
	 *
	 * Daher können Tests grundsätzlich nur von denen gestartet werden, die Schreibrechte auf
	 * den Test haben (d.h. nur von Erstellern). Einzige Ausnahme ist, falls der Test im
	 * Testdurchführungsbereich ist ($this->mayAccessTestAsParticipant); in diesem Fall haben
	 * genau jene Zugriff darauf, die im Prüfungsraum sind.
	 *
	 * Administratoren haben ungeachtet dessen immer Zugriff auf die Tests.
	 *
	 * @iliashook ilTestPasswordChecker.isTestAccessible
	 * @param $rbacsystem
	 * @param ilObjTest $test
	 * @return bool
	 */

	public function isTestAccessible($rbacsystem, $test)
	{
		global $ilSetting;

		if ($ilSetting->get("ur_disable_test_protection") == 1) {
			return true;
		}

		$examPlugin = $this->hooks->getExtension('Exam');

		if ($examPlugin && $examPlugin->mayAccessTestAsParticipant($test)) {
			return true;
		} else {
			// Ersteller/Schreibberechtiger hat immer Zugriff.
			return $rbacsystem->checkAccess('write', $test->getRefId());
		}
	}
}
