<?php

class urAdminExtension extends urExtension {
	/**
	 * @var urHooks
	 */

	private $hooks;

	public function __construct($hooks, $config)
	{
		$this->hooks = $hooks;
		$hooks->getExtension('Translations')->addTranslation("urepruefung", "E-Prüfungen");
	}

	/**
	 * @iliashook ilObjSystemFolderGUI.getAdminTabs
	 * @param ilObjSystemFolderGUI $gui
	 * @param ilTabsGUI tabs
	 * @param ilCtrl ctrl
	 */

	public function addSystemFolderTabs($gui, $tabs, $ctrl)
	{
		global $rbacsystem;

		if ($rbacsystem->checkAccess("write", $gui->object->getRefId())) {
			$tabs->addTarget("urepruefung",
				$ctrl->getLinkTarget($gui, "urepruefung"), "urepruefung", get_class($gui));
		}
	}

	/**
	 * @iliashook ilObjSystemFolderGUI.executeCommand
	 * @param ilObjSystemFolderGUI $gui
	 * @param array $data
	 * @param string $cmd
	 * @return bool
	 */

	public function executeSystemFolderCommand($gui, $data, $cmd)
	{
		switch ($cmd) {
			case "urepruefung":
				$rbacsystem = $data->rbacsystem;
				$lng = $data->lng;
				$ilCtrl = $data->ctrl;
				$ilSetting = $data->settings;
				$tpl = $data->tpl;
				$ilErr = $data->error;

				if (!$rbacsystem->checkAccess("visible,read", $data->object->getRefId())) {
					$ilErr->raiseError($data->lng->txt("permission_denied"), $ilErr->MESSAGE);
				}

				$ilTabs = $data->tabs;
				$ilTabs->activateTab("urepruefung");

				include_once("Services/Form/classes/class.ilPropertyFormGUI.php");
				$form = new ilPropertyFormGUI();

				$this->createAdminUI($form, $ilSetting);

				$form->addCommandButton("saveUREPruefungSettings", $lng->txt("save"));

				$form->setFormAction($ilCtrl->getFormAction($gui));

				$tpl->setContent($form->getHTML());
				return true;

			case "saveUREPruefungSettings":
				$rbacsystem = $data->rbacsystem;
				$ilErr = $data->error;

				if (!$rbacsystem->checkAccess("visible,read", $data->object->getRefId())) {
					$ilErr->raiseError($data->lng->txt("permission_denied"), $ilErr->MESSAGE);
				}

				$this->saveAdminUI();

				ilUtil::sendSuccess($data->lng->txt("msg_obj_modified"), true);

				$data->ctrl->redirect($gui, "urepruefung");
				return true;
		}

		return false;
	}

	/**
	 * @param ilPropertyFormGUI $form
	 * @param ilSetting $ilSetting
	 */

	private function createAdminUI($form, $ilSetting)
	{
		$sectionOrder = array(
			'raumsteuerung',
			'testschutz',
			'antwortversionen',
			'datenintegrität');

		$sections = array();

		foreach ($this->hooks->getExtensions() as $name => $plugin) {
			if (method_exists($plugin, 'createSettingGUI')) {
				$gui = $plugin->createSettingGUI();
				if ($gui) {
					foreach ($gui as $section => $components) {
						if (!isset($sections[$section])) {
							$sections[$section] = array();
						}

						$sections[$section] = array_merge($sections[$section], $components);
					}
				}
			}
		}

		uksort($sections, function($a, $b) use ($sectionOrder) {
			return array_search(strtolower($a), $sectionOrder) - array_search(strtolower($b), $sectionOrder);
		});

		foreach ($sections as $name => $components) {
			$sh = new ilFormSectionHeaderGUI();
			$sh->setTitle($name);
			$form->addItem($sh);

			foreach ($components as $component) {
				$component->build($form);
			}
		}
	}

	private function saveAdminUI()
	{
		foreach ($this->hooks->getExtensions() as $name => $plugin) {
			if (method_exists($plugin, 'createSettingGUI')) {
				$gui = $plugin->createSettingGUI();

				foreach ($gui as $section => $components) {
					foreach ($components as $component) {
						$component->save();
					}
				}
			}
		}
	}
}
