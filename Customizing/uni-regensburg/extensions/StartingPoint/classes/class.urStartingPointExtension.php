<?php

class urStartingPointExtension extends urExtension {

	private $hooks;

	public function __construct($hooks, $config)
	{
		$this->hooks = $hooks;
	}

	/**
	 *
	 * Liefert die URL, die ein Nutzer nach dem Login sehen soll, oder false, falls das normale
	 * ILIAS-Verhalten zum Tragen kommen soll.
	 *
	 * @iliashook ilUserUtil.getStartingPointAsUrl
	 * @return bool|string URL oder false
	 */

	public function getStartingPointHook()
	{
		$examPlugin = $this->hooks->getExtension('Exam');
		if ($examPlugin) {
			if ($examPlugin->isPrivilegedAssessmentParticipant()) {
				return $examPlugin->getKlausurbereichURL();
			}
		}

		return false;
	}

}
