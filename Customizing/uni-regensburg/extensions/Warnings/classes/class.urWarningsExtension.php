<?php

class urWarningsExtension extends urExtension {

	private $hooks;

	public function __construct($hooks, $config) {
		$this->hooks = $hooks;
	}

	/**
	 * @iliashook ilMainMenuGUI.getHTML
	 * @return string
	 */

	public function executeMainMenuHtml() {
		if (!$this->isAdministrator()) {
			return ''; // Für Nicht-Adminstratoren werden in keinem Fall UR-interne Warnhinweise angezeigt.
		}
		$html = array();
		$result = $this->getGlobalWarnings();
		foreach ($result as $type => $boxes) {
			$li = array();
			foreach ($boxes as $box) {
				array_push($li, "<li>" . $box . "</li>");
			}
			array_push($html, $this->warningBox($type, "<ul>" . implode("", $li) . "</ul>"));
		}
		$html = implode("<br>", $html);

		return <<<EOT
    		<div id="ur-warnings" class="container-fluid" style="padding: 2em;">
				$html
    		</div>
			<script>
				$(function() {
				    $("#mainspacekeeper").prepend($("#ur-warnings"));
				});
			</script>
EOT;
	}

	/**
	 * @iliashook getGlobalWarnings
	 * @return array
	 */

	protected function getGlobalWarnings()
	{
		$warnings = $this->getKlausurbereichWarnings();

		if (empty($warnings)) {
			return array();
		}

		$messages = array();

		global $ilSetting;
		if ($ilSetting->get("ur_disable_test_protection")) {
			array_push($warnings, array("warning", "Erweiterter Testschutz ist aufgehoben."));
		}
		if ($ilSetting->get("ur_disable_room_isolation")) {
			array_push($warnings, array("warning", "Prüfungszugänge campusweit verfügbar."));
		}

		foreach ($warnings as $w) {
			list($type, $text) = $w;
			if (!isset($messages[$type])) {
				$messages[$type] = array();
			}
			array_push($messages[$type], $text);
		}

		$moreWarnings = $this->hooks->executeHooks('urWarningsExtension.getGlobalWarnings');
		if ($moreWarnings) {
			foreach (call_user_func_array('array_merge', $moreWarnings) as $w) {
				list($type, $text) = $w;
				if (!isset($messages[$type])) {
					$messages[$type] = array();
				}
				array_push($messages[$type], $text);
			}
		}

		return $messages;
	}

	private function getKlausurbereichWarnings()
	{
		$warnings = array();

		if ($this->isAdministrator()) {
			$examPlugin = $this->hooks->getExtension('Exam');
			if ($examPlugin) {
				global $tree;

				foreach ($examPlugin->getTestAreaIds() as $catId) {
					$n = count($tree->getChildsByType($catId, "tst"));
					if ($n > 0) {
						$nd = $tree->getNodeData($catId);
						$title = $nd["title"];
						$url = ilLink::_getStaticLink($catId);
						array_push($warnings,
							array('info', 'Raum unsicher. Öffentlich zugreifbare Tests im Bereich ' .
								'<a href="' . $url . '">' . $title . '</a>.'));
					}
				}

				global $ilSetting;

				if ($ilSetting->get("ur_enable_autoprint") != 0) {
					array_push($warnings, array('info', 'BIB3 unsicher. Automatisches Drucken ist aktiviert.'));
				}

				if ($ilSetting->get("ur_autoboot_bib3") != 0) {
					array_push($warnings, array('info', 'Automatischer Boot in Linux/SEB für BIB3 aktiviert.'));
				}
			}
		}

		return $warnings;
	}

	/**
	 * @param string $type "info" oder "error"
	 * @param string $text
	 * @return string
	 */

	private function warningBox($type, $text)
	{
		if ($type == "info") {
			$color = "#FFFACD";
			$dashColor = "#FFFF80";
		} else {
			$color = "#FFD2D2";
			$dashColor = "#FA8072";
		}
		return <<<EOT
			<table style="width: 100%; border: 3px dashed $dashColor;">
				<tr style="background: $color;  color: #D8000C; height: 2em;"><td style="padding: 1em;">
					$text
				</td></tr>
			</table>
EOT;
	}
}
